@extends('layouts.master')


@section('search')
    <form method="get" action="{{ tokenRoute('userFilter') }}">
        {!! getTokenToForm() !!}
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable mdl-textfield--floating-label mdl-textfield--align-right">

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
                    <label class="mdl-button mdl-js-button mdl-button--icon" for="search">
                        <i class="material-icons">search</i>
                    </label>
                    <div class="mdl-textfield__expandable-holder">
                        <input class="mdl-textfield__input" type="text" id="search" name="search">
                        <label class="mdl-textfield__label" for="search">Enter your query...</label>
                    </div>
                </div>                    
            </div>                        
    </form>
@endsection

@section('title', 'Users:')



@section('content')

<!--
<dialog id="confirm-delete-dialog" class="mdl-dialog">
    <h4 class="mdl-dialog__title">Do you really want to delete this user?</h4>

    <div class="mdl-dialog__actions">
        <button type="button" class="mdl-button delete">Yes</button>
        <button type="button" class="mdl-button close">No</button>
    </div>
</dialog>
-->



@foreach ($paginator['data']['items'] as $item)

<div class="user-card turn-card-wide mdl-card mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet">
    <div class="mdl-card__title">
        <h2 class="mdl-card__title-text">
            {{ $item->username }}
        </h2>
    </div>


    <div class="mdl-grid w100">
        
        <div class="mdl-cell mdl-cell--4-col mdl-cell--12-col-tablet">    
            @if ($item->image['thumbnail'])
            <!--<div class="mdl-card__media">-->
                <img class="img-responsive" src="{{ $item->image['thumbnail'] }}" alt="">
            <!--</div>-->
            @endif    
        </div>
        
        <div class="mdl-cell mdl-cell--8-col mdl-cell--12-col-tablet">    

            <div class="mdl-card__supporting-text">                

                <table>
                    <tbody>

                        <tr>
                            <th class="mdl-data-table__cell--non-numeric">Email:</th>
                            <td class="mdl-data-table__cell--non-numeric">{{ $item->email }}</td>
                        </tr>

                        <tr>
                            <th class="mdl-data-table__cell--non-numeric">Birthday:</th>
                            <td class="mdl-data-table__cell--non-numeric">{{ $item->birthday }}</td>
                        </tr>

                        <tr>
                            <th class="mdl-data-table__cell--non-numeric">Rating:</th>
                            <td class="mdl-data-table__cell--non-numeric">{{ $item->rating }}</td>
                        </tr>

                        <tr>
                            <th class="mdl-data-table__cell--non-numeric">School:</th>
                            <td class="mdl-data-table__cell--non-numeric">{{ $item->school_name }}</td>
                        </tr>                

                        <tr>
                            <th class="mdl-data-table__cell--non-numeric">Created:</th>
                            <td class="mdl-data-table__cell--non-numeric">{{ $item->created_at }}</td>
                        </tr>                         

                    </tbody>
                </table>

            </div>            
            
        </div>
        
            <div class="fullwidth">
                <p>{{ str_limit($item->description, 200, '...') }}</p>
            </div>        
        
    </div>
    
    <!--
    <div class="mdl-card__actions mdl-card--border">
        <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
            Edit
        </a>
    </div>-->
    <div class="mdl-card__menu">
        
    
        
        <a class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" href="{{ TokenRoute('userEdit', ['user_id' => $item->id]) }}">
            <i class="material-icons">edit</i>
        </a>        
        

        
        <form class="delete-wrap" action="{{ TokenRoute('userDelete', ['user_id' => $item->id]) }}" method="post">
            {!! getTokenToForm() !!}
            <button type="submit" class="delete-submit mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect delete-dialog">
                <i class="material-icons">delete</i>
            </button>
        </form>
    </div>
</div>

@endforeach



@if ( 1 < $paginator['data']['pages'] )
<div class="mdl-cell mdl-cell--12-col">
    @for ($i = 0; $i < $paginator['data']['pages']; $i++)
        <a href="{{ TokenRoute('userList') }}&offset={{ $i * $paginator['data']['limit'] }}">
            <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--colored">
                {{ $i + 1 }}
            </button>
        </a>
    @endfor
</div>

@endif 


  <script>
    //var dialog = document.querySelector('#confirm-delete-dialog');
    //var showDialogButton = document.querySelectorAll('.delete-dialog');
    
    var deleteSubmit = document.querySelectorAll('.delete-submit');
    
    for(i = 0; i < deleteSubmit.length;i++) {
        
        console.log('hell');
        
        deleteSubmit[i].addEventListener('click', function(event){
            
            result = window.confirm("Do you really want do delete this user?");
            
            if(result){
                return true;
            }
            else {
                event.preventDefault();
                return false;
            }
            
        })
    }        
    
    
    //result = window.confirm(message);
    
    //console.log(showDialogButton);
    
    
    /*
    for(i = 0; i < showDialogButton.length;i++) {
      showDialogButton[i].addEventListener('click', function(event){
          dialog.showModal();
          //event.stopPropagation();
          return false;
          //event.preventDefault();
      });
    }    
    
    if (! dialog.showModal) {
      dialogPolyfill.registerDialog(dialog);
    }
    */
    
    /*
    showDialogButton.addEventListener('click', function() {
        console.log('hello to all');
      dialog.showModal();
    });*/
    
    /*
    dialog.querySelector('.close').addEventListener('click', function() {        
        dialog.close();        
        return false;
    });
    
    dialog.querySelector('.delete').addEventListener('click', function() {        
        dialog.close();        
        return true;
    });    
    */
    
    
  </script>


@endsection
