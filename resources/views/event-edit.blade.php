@extends('layouts.master')

@section('title', 'Event:')

@section('content')
<div class="mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-grid">
    <div class="container">
        
        @if ( isset($errorMessages) && count($errorMessages) > 0)
            <div class="error-message">
            @foreach ($errorMessages as $message)
                <p>{{ $message }}</p>
            @endforeach
            </div>

        @endif
        
        
        <form action="{{ TokenRoute('eventSave', ['event_id' => $event->id]) }}" method="post">

            {!! getTokenToForm() !!}
            
            <div class="mdl-textfield mdl-textfield--full-width mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="name" value="{{ $event->name }}" name="name" />
                <label class="mdl-textfield__label" for="name">Name</label>
            </div>

            <div class="mdl-textfield mdl-textfield--full-width mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows= "5" id="description" name="description">{{ $event->description }}</textarea>
                <label class="mdl-textfield__label" for="description">Description</label>
            </div>

            <div class="mdl-textfield mdl-textfield--full-width mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="date" value="{{ $event->date }}"  name="date" />
                <label class="mdl-textfield__label" for="date">Date</label>
            </div>

            <div class="mdl-textfield mdl-textfield--full-width mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="capacity"  value="{{ $event->capacity }}"  name="capacity" />
                <label class="mdl-textfield__label" for="capacity">Capacity</label>
            </div>

            <!--
            <div class="mdl-textfield mdl-textfield--full-width mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="indoor"  value="{{ $event->indoor }}" name="indoor" />
                <label class="mdl-textfield__label" for="indoor">Indoor</label>
            </div>
            -->
            
            <!-- Simple Select -->
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select mdl-textfield--full-width mdl-js-textfield">
                
                <?php
                
                $options = [
                    0   =>  'No',
                    1   =>  'Yes',
                ];
                
                ?>
                
                <select id="indoor" name="indoor" class="mdl-textfield__input">
                    <!--<option>---</option>-->
                    @foreach ($options as $key => $option)
                    <option @if ($event->indoor == $key) selected="selected" @endif value="{{ $key }}">{{ $option }}</option>
                    @endforeach
                </select>
                
                <label for="indoor" class="mdl-textfield__label">Indoor</label>
                

            </div>            
            
            
            <!-- private -->
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select mdl-textfield--full-width mdl-js-textfield">
                
                <?php
                
                $options = [
                    0   =>  'No',
                    1   =>  'Yes',
                ];
                
                ?>
                
                <select id="private" name="private" class="mdl-textfield__input">
                    <!--<option>---</option>-->
                    @foreach ($options as $key => $option)
                    <option @if ($event->private == $key) selected="selected" @endif value="{{ $key }}">{{ $option }}</option>
                    @endforeach
                </select>
                
                <label for="private" class="mdl-textfield__label">Private</label>
                

            </div>
            <!-- /private -->
            
            
            <!--
            <div class="mdl-textfield mdl-textfield--full-width mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="private"  value="{{ $event->private }}" name="private" />
                <label class="mdl-textfield__label" for="private">Private</label>
            </div>            
            -->

            <div class="mdl-textfield mdl-textfield--full-width mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="street" value="{{ $event->street }}" name="street" />
                <label class="mdl-textfield__label" for="street">Street</label>
            </div>

            <div class="mdl-textfield mdl-textfield--full-width mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="city" value="{{ $event->city }}" name="city" />
                <label class="mdl-textfield__label" for="city">City</label>
            </div>

            <div class="mdl-textfield mdl-textfield--full-width mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="number"  value="{{ $event->number }}" name="number" />
                <label class="mdl-textfield__label" for="number">Number</label>
            </div>

            <div class="mdl-textfield mdl-textfield--full-width mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="zip" value="{{ $event->zip }}" name="zip" />
                <label class="mdl-textfield__label" for="zip">Zip</label>
            </div>

<!--            
            <div class="mdl-textfield mdl-textfield--full-width mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="gsp" value="{{ $event->gsp }}" name="gsp" />
                <label class="mdl-textfield__label" for="gsp">GSP</label>
            </div>
-->


            <div class="fullwidth">
                <button class="mdl-button mdl-js-button mdl-button--raised">
                    Save
                </button>
            </div>

        </form>
    </div>
</div>
@endsection

