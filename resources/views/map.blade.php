<!-- resources/views/quote.blade.php -->
<html>
    <head>
        <title>Motivaitonal — Your daily source of motivation!</title>


        <script src="https://fb.me/react-0.14.3.js"></script>
        <script src="https://fb.me/react-dom-0.14.3.js"></script>

        <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.light_blue-orange.min.css" />
        <link href='http://fonts.googleapis.com/css?family=Alegreya:400,700|Roboto+Condensed' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div id="map-canvas" style="height:600px"></div>
            </div>
        </div>

        <script>
                    function initMap(){

                    var mapOptions = {
                    zoom: 4,
                            center: new google.maps.LatLng({{$items[0]->gps}})
                    }
                    var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
                            @foreach($items as $item)
                            var marker{{$item->id}} = new google.maps.Marker({
                    position: new google.maps.LatLng({{$item->gps}}),
                            map: map,
                            title: "{{$item->id}}"
                    });
                            @endforeach
                    }
        </script>

        <script src="https://maps.googleapis.com/maps/api/js?callback=initMap" async defer></script>


        <script>
                    /*
                     function loadScript(src,callback){

                     var script = document.createElement("script");
                     script.type = "text/javascript";
                     if(callback)script.onload=callback;
                     document.getElementsByTagName("head")[0].appendChild(script);
                     script.src = src;
                     }

                     loadScript('http://maps.googleapis.com/maps/api/js?v=3&sensor=false&callback=initialize',
                     function(){log('google-loader has been loaded, but not the maps-API ');});

                     function initialize() {
                     log('maps-API has been loaded, ready to use');
                     var mapOptions = {
                     zoom: 8,
                     center: new google.maps.LatLng(-34.397, 150.644),
                     mapTypeId: google.maps.MapTypeId.ROADMAP
                     };
                     map = new google.maps.Map(document.getElementById('map_canvas'),
                     mapOptions);
                     }

                     function log(str){
                     document.getElementsByTagName('pre')[0].appendChild(document.createTextNode('['+new Date().getTime()+']\n'+str+'\n\n'));
                     }
                     */
        </script>
    </body>
</html>