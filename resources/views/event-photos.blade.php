@extends('layouts.master')


@section('title')
{{ $event->name }} - photos
@endsection


@section('content')

<!--
<dialog id="confirm-delete-dialog" class="mdl-dialog">
    <h4 class="mdl-dialog__title">Do you really want to delete this user?</h4>

    <div class="mdl-dialog__actions">
        <button type="button" class="mdl-button delete">Yes</button>
        <button type="button" class="mdl-button close">No</button>
    </div>
</dialog>
-->

<div class="mdl-card mdl-cell mdl-cell--12-col">
    <div class="mdl-grid w100">
        <div class="mdl-cell mdl-cell--12-col">
            <form action="{{ TokenRoute('eventPhotosSave', ['event_id' => $event->id]) }}" method="post"  enctype="multipart/form-data" autocomplete="off">
                {!! getTokenToForm() !!}
                <div class="mdl-textfield--full-width  element file mdl-file mdl-js-file mdl-file--floating-label">
                    <input type="file" name="image[]" id="image" multiple="multiple">
                    <label for="image">Upload photo</label>
                </div>

                <div class="fullwidth">
                    <button class="mdl-button mdl-js-button mdl-button--raised">
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


@foreach ($photos as $item)

<div class="image-card mdl-card mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-shadow--2dp">
    

        <div class="image-card-in">
            <!--<div class="mdl-card__media">-->
            <img class="img-responsive" src="{{ $item->thumbnail }}" alt="">
            <!--</div>-->
        </div>    


    

    <!--
    <div class="mdl-card__actions mdl-card--border">
        <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
            Edit
        </a>
    </div>-->
    <div class="mdl-card__menu">


        <!--
                <a class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" href="{{ TokenRoute('userEdit', ['user_id' => $item->id]) }}">
                    <i class="material-icons">edit</i>
                </a>
        -->



        <form class="delete-wrap" action="{{ TokenRoute('eventDeleteImage', ['event_id' => $event->id, 'image_id' => $item->id]) }}" method="post">
            {!! getTokenToForm() !!}
            <button type="submit" class="delete-button mdl-button mdl-js-button mdl-button--fab mdl-button--colored delete-dialog">
                <i class="material-icons">delete</i>
            </button>
        </form>
    </div>
</div>

@endforeach






<script>
    //var dialog = document.querySelector('#confirm-delete-dialog');
    //var showDialogButton = document.querySelectorAll('.delete-dialog');

    var deleteSubmit = document.querySelectorAll('.delete-button');

    for (i = 0; i < deleteSubmit.length; i++) {

        console.log('hell');

        deleteSubmit[i].addEventListener('click', function (event) {

            result = window.confirm("Do you really want do delete this image?");

            if (result) {
                return true;
            } else {
                event.preventDefault();
                return false;
            }

        })
    }


    //result = window.confirm(message);

    //console.log(showDialogButton);


    /*
     for(i = 0; i < showDialogButton.length;i++) {
     showDialogButton[i].addEventListener('click', function(event){
     dialog.showModal();
     //event.stopPropagation();
     return false;
     //event.preventDefault();
     });
     }
     
     if (! dialog.showModal) {
     dialogPolyfill.registerDialog(dialog);
     }
     */

    /*
     showDialogButton.addEventListener('click', function() {
     console.log('hello to all');
     dialog.showModal();
     });*/

    /*
     dialog.querySelector('.close').addEventListener('click', function() {
     dialog.close();
     return false;
     });
     
     dialog.querySelector('.delete').addEventListener('click', function() {
     dialog.close();
     return true;
     });
     */


</script>


@endsection
