@extends('layouts.master')

@section('search')
    <form method="get" action="{{ tokenRoute('eventFilter') }}">
        {!! getTokenToForm() !!}
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable mdl-textfield--floating-label mdl-textfield--align-right">

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
                    <label class="mdl-button mdl-js-button mdl-button--icon" for="search">
                        <i class="material-icons">search</i>
                    </label>
                    <div class="mdl-textfield__expandable-holder">
                        <input class="mdl-textfield__input" type="text" id="search" name="search">
                        <label class="mdl-textfield__label" for="search">Enter your query.. press enter</label>
                    </div>
                </div>                    
            </div>                        
    </form>
@endsection


@section('title', 'Events:')


@section('content')

@foreach ($paginator['data']['items'] as $item)

<div class="event-card turn-card-wide mdl-card mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet @if ( 1 == $item->canceled ) item-canceled @endif">
    <div class="mdl-card__title">
        <h2 class="mdl-card__title-text">
            {{ $item->name }}{{ $item->canceled }}
        </h2>
    </div>

    <div class="mdl-grid w100">

        <div class="mdl-cell mdl-cell--12-col">

            <div class="fullwidth">
                <p>{{ str_limit($item->description, 200, '...') }}</p>
            </div>     

            <div class="mdl-card__supporting-text">        

                <table>
                    <tbody>

                        <tr>
                            <th class="mdl-data-table__cell--non-numeric">Date:</th>
                            <td class="mdl-data-table__cell--non-numeric">{{ $item->date }}</td>
                            <th class="mdl-data-table__cell--non-numeric">Street:</th>
                            <td class="mdl-data-table__cell--non-numeric">{{ $item->street }}</td>
                        </tr>

                        <tr>
                            <th class="mdl-data-table__cell--non-numeric">Capacity:</th>
                            <td class="mdl-data-table__cell--non-numeric">{{ $item->capacity }}</td>
                            <th class="mdl-data-table__cell--non-numeric">City:</th>
                            <td class="mdl-data-table__cell--non-numeric">{{ $item->city }}</td>
                        </tr>

                        <tr>
                            <th class="mdl-data-table__cell--non-numeric">Indoor:</th>
                            <td class="mdl-data-table__cell--non-numeric">{{ (int) $item->indoor }}</td>
                            <th class="mdl-data-table__cell--non-numeric">Number:</th>
                            <td class="mdl-data-table__cell--non-numeric">{{ $item->number }}</td>
                        </tr>

                        <tr>
                            <th class="mdl-data-table__cell--non-numeric">Private:</th>
                            <td class="mdl-data-table__cell--non-numeric">{{ (int) $item->private }}</td>
                            <th class="mdl-data-table__cell--non-numeric">Zip:</th>
                            <td class="mdl-data-table__cell--non-numeric">{{ $item->zip }}</td>
                        </tr>

                        <tr>
                            <th class="mdl-data-table__cell--non-numeric">School:</th>
                            <td class="mdl-data-table__cell--non-numeric">@if (isset($item->school->name)) {{ $item->school->name }} @endif</td>
                            <th class="mdl-data-table__cell--non-numeric">GPS:</th>
                            <td class="mdl-data-table__cell--non-numeric">{{ $item->gps }}</td>
                        </tr>
                        
                       <tr>
                            <th class="mdl-data-table__cell--non-numeric">Participiants:</th>
                            <td class="mdl-data-table__cell--non-numeric">{{ $item->participiants }}</td>
                            <th class="mdl-data-table__cell--non-numeric">Registered:</th>
                            <td class="mdl-data-table__cell--non-numeric">{{ $item->registred }}</td>
                        </tr>                        

                    </tbody>
                </table>
            </div>
        </div>
    </div>    
    
    
    @if ( 1 == $item->canceled )
    <div class="mdl-card__actions mdl-card--border">    
        <div class="mdl-grid">
            <div class="mdl-cell mdl-cell--12-col">        
                Canceled        
            </div>
        </div>
    </div>
    @endif
    
    <div class="mdl-card__menu">
        
        <a class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" href="{{ TokenRoute('eventPhotos', ['event_id' => $item->id]) }}">
            <i class="material-icons">party_mode</i>
        </a>           
        
        <a class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" href="{{ TokenRoute('eventEdit', ['event_id' => $item->id]) }}">
            <i class="material-icons">edit</i>
        </a>

        
        @if ( 1 == $item->canceled )
            <form class="delete-wrap" action="{{ TokenRoute('eventRestore', ['event_id' => $item->id]) }}" method="post">
                {!! getTokenToForm() !!}
                <button type="submit" class="delete-submit mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect delete-dialog">
                    <i class="material-icons">backup</i>
                </button>
            </form>        
        @else
            <form class="delete-wrap" action="{{ TokenRoute('eventDelete', ['event_id' => $item->id]) }}" method="post">
                {!! getTokenToForm() !!}
                <button type="submit" class="delete-submit mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect delete-dialog">
                    <i class="material-icons">delete</i>
                </button>
            </form>        
        @endif

    </div>
</div>

@endforeach


@if ( 1 < $paginator['data']['pages'] )
<div class="mdl-cell mdl-cell--12-col">
    @for ($i = 0; $i < $paginator['data']['pages']; $i++)
        <a href="{{ TokenRoute('eventList') }}&offset={{ $i * $paginator['data']['limit'] }}">
            <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--colored">
                {{ $i + 1 }}
            </button>
        </a>
    @endfor
</div>
@endif


@endsection



