@extends('layouts.master')

@section('title', 'User:')

@section('content')
<div class="mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-grid">
    <div class="container">

        @if ( isset($errorMessages) && count($errorMessages) > 0)
            <div class="error-message">
            @foreach ($errorMessages as $message)
                <p>{{ $message }}</p>
            @endforeach
            </div>

        @endif


        @if ($user->image['thumbnail'])
            <div class="mdl-cell mdl-cell--12-col">
                <form method="post" action="{{ TokenRoute('userDeleteImage', ['user_id' => $user->id]) }}">
                    
                    {!! getTokenToForm() !!}
                    
                    <div class="user-image">

                        <button type="submit" class="delete-button mdl-button mdl-js-button mdl-button--fab mdl-button--colored">
                            <i class="material-icons">delete</i>
                        </button>                

                        <img class="img-responsive" src="{{ $user->image['thumbnail'] }}" alt="">

                    </div>
                </form>
            </div>
        @endif


        <form action="{{ tokenRoute('userSave', ['user_id' => $user->id]) }}" method="post"  enctype="multipart/form-data" autocomplete="off">

            
            {!! getTokenToForm() !!}
            
            <div class="mdl-textfield--full-width  element file mdl-file mdl-js-file mdl-file--floating-label">
              <input type="file" name="image" id="image">
              <label for="image">Profile image</label>
            </div>            
            
            <!--
            <div class="mdl-textfield mdl-textfield--full-width mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="file" id="image" name="image" />
                <label class="mdl-textfield__label" for="image">Profile image</label>
            </div>  
            -->

            <!--
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--file">
              <input class="mdl-textfield__input" placeholder="File" type="text" id="uploadFile" readonly/>
              <div class="mdl-button mdl-button--primary mdl-button--icon mdl-button--file">
                <i class="material-icons">attach_file</i><input type="file" id="uploadBtn">
              </div>
            </div>
            -->

            
            

            <div class="mdl-textfield mdl-textfield--full-width mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="username" name="username" value="{{ $user->username }}" />
                <label class="mdl-textfield__label" for="username">Username</label>
            </div>

            <div class="mdl-textfield mdl-textfield--full-width mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="email" name="email" value="{{ $user->email }}" />
                <label class="mdl-textfield__label" for="email">Email</label>
            </div>

            <div class="mdl-textfield mdl-textfield--full-width mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="password" id="password" name="password" value="" readonly onfocus="this.removeAttribute('readonly');" />
                <label class="mdl-textfield__label" for="password">Password</label>
            </div>

            <div class="mdl-textfield mdl-textfield--full-width mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="birthday" id="birthday" name="birthday" value="{{ $user->birthday }}" />
                <label class="mdl-textfield__label" for="birthday">Birthday</label>
            </div>

            <div class="mdl-textfield mdl-textfield--full-width mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows= "5" id="description" name="description" >{{ $user->description }}</textarea>
                <label class="mdl-textfield__label" for="description">Description</label>
            </div>

            <!-- Simple Select -->
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select mdl-textfield--full-width mdl-js-textfield">
                
                <select id="school" name="school" class="mdl-textfield__input">
                    <option>---</option>
                    @foreach ($schools as $school)
                    <option @if ($school->id == $user->school) selected="selected" @endif   value="{{ $school->id }}">{{ $school->name }}</option>
                    @endforeach
                </select>
                
                <label for="school" class="mdl-textfield__label">School</label>
                
                <!--
                <input class="mdl-textfield__input" type="text" id="sample1" value="belarus" readonly tabIndex="-1">
                <label for="sample1" class="mdl-textfield__label">School</label>
                <ul for="sample1" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">

                    @foreach ($schools as $school)
                        <li class="mdl-menu__item">{{ $school->name }}</li>
                    @endforeach

                </ul>
                -->
            </div>

            <div class="fullwidth">
                <button class="mdl-button mdl-js-button mdl-button--raised">
                    Save
                </button>
            </div>

            <!--
            <div class="mdl-textfield mdl-textfield--full-width mdl-js-textfield">
                <input class="mdl-textfield__input" type="birthday" id="userpass" />
                <label class="mdl-textfield__label" for="birthday">Rating</label>
            </div>
            -->

            <!--
            <div class="mdl-textfield mdl-textfield--full-width mdl-js-textfield">
                <input class="mdl-textfield__input" type="birthday" id="birthday" />
                <label class="mdl-textfield__label" for="birthday">{{ $user->birthday }}</label>
            </div>
            -->
        </form>
    </div>
</div>

<script>
/*
    var deleteSubmit = document.querySelector('.delete-button');
        
    deleteSubmit.addEventListener('click', function(event){
            
        result = window.confirm("Do you really want do delete profile image?");
            
        if(result){
            return true;
        }
        else {
            event.preventDefault();
            return false;
        }
            
    })
     
*/
</script>


@endsection