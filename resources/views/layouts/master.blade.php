<!doctype html>
<!--
  Material Design Lite
  Copyright 2015 Google Inc. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      https://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
        <title>Turn App</title>

        <!-- Add to homescreen for Chrome on Android -->
        <meta name="mobile-web-app-capable" content="yes">
        <link rel="icon" sizes="192x192" href="images/android-desktop.png">

        <!-- Add to homescreen for Safari on iOS -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-title" content="Material Design Lite">
        <link rel="apple-touch-icon-precomposed" href="images/ios-desktop.png">

        <!-- Tile icon for Win8 (144x144 + tile color) -->
        <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
        <meta name="msapplication-TileColor" content="#3372DF">

        <link rel="shortcut icon" href="images/favicon.png">

        <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
        <!--
        <link rel="canonical" href="http://www.example.com/">
        -->

        <link href='http://fonts.googleapis.com/css?family=Alegreya:400,700|Roboto+Condensed' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.light_blue-orange.min.css">
        <link rel="stylesheet" href="/css/styles.css">
        <link rel="stylesheet" href="/css/_file.css">

        <script src="/polyfill/dialog-polyfill.js"></script>
        <link rel="stylesheet" type="text/css" href="/polyfill/dialog-polyfill.css" />        

        <style>
            #view-source {
                position: fixed;
                display: block;
                right: 0;
                bottom: 0;
                margin-right: 40px;
                margin-bottom: 40px;
                z-index: 900;
            }
        </style>
    </head>
    <body>
        <div class="turn-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
            <header class="turn-header mdl-layout__header mdl-color--grey-100 mdl-color-text--grey-600">
                <div class="mdl-layout__header-row">
                    <span class="mdl-layout-title">@yield('title')</span>
                    <div class="mdl-layout-spacer"></div>

                    @yield('search')
                    

                </div>
            </header>
            <div class="turn-drawer mdl-layout__drawer mdl-color--blue-grey-900 mdl-color-text--blue-grey-50">
                <header class="turn-drawer-header">
                    <img src="/images/user.jpg" class="turn-avatar">
                    <div class="turn-avatar-dropdown">
                        <a href="{{ TokenRoute('adminLogout') }}"><span>Logout</span></a>

                        <!--
                        <div class="mdl-layout-spacer"></div>
                        <button id="accbtn" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">
                            <i class="material-icons" role="presentation">input</i>
                            <span class="visuallyhidden">Accounts</span>
                        </button>
                        -->

                        <!--
                        <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="accbtn">
                            
                            <li class="mdl-menu__item">hello@example.com</li>
                            <li class="mdl-menu__item">info@example.com</li>                            
                            <li class="mdl-menu__item"><i class="material-icons">add</i>Add another account...</li>
                            
                        </ul>
                        -->

                    </div>
                </header>
                <nav class="turn-navigation mdl-navigation mdl-color--blue-grey-800">
                    <a class="mdl-navigation__link" href="{{ TokenRoute('eventList') }}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">flag</i>Events</a>
                    <a class="mdl-navigation__link" href="{{ TokenRoute('userList') }}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">people</i>Users</a>
                </nav>
            </div>

            <main class="mdl-layout__content mdl-color--grey-100">
                <div class="mdl-grid turn-content">

                    <!--
                    <div class="mdl-cell mdl-cell--12-col mdl-grid">
                        <div class="container">
                    -->
                    @yield('content')
                    <!--
                </div>
            </div>
                    -->



                </div>
            </main>

        </div>


        <script src="https://code.getmdl.io/1.1.3/material.min.js"></script>
        <script src="/js/file.js" type="text/javascript"></script>
    </body>
</html>
