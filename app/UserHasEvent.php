<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHasEvent extends Model {
    
    const CREATOR = 1;
    const VISITOR = 0;

    public $timestamps = false;
    protected $table = 'user_has_event';
    protected $fillable = ['user_id', 'event_id', 'user_has_event'];

}

?>