<?php

namespace App\Auth;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {
    use Authenticatable, CanResetPassword;
    /**
* The database table used by the model.
*
* @var string
*/
    
    use SoftDeletes;
    
    protected $table = 'users';
    
    
    //protected $dates = ['created_at', 'updated_at', 'deleted_at', 'date'];
    
    /**
* The attributes that are mass assignable.
*
* @var array
*/
    protected $fillable = [
        'username',
        'email',
        'password',
        'id_users_facebook',
        'id_users_twitter',
        'birthday',
        'description',
        'school',
        'image',
        'thumbnail',
	    'device_token'
    ];


    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = ['password', 'remember_token', 'id_users_facebook', 'id_users_twitter', 'thumbnail', 'user_has_event', 'user_id', 'thumbnail', 'originalImage'];

    protected $appends = ['originalImage', 'rating', 'school_detail'];

    //protected $visible = ['image'];


    public function getSchoolDetailAttribute(){        
        $school = \App\Models\School::where('id', $this->attributes['school'])->first();
        return $school;
    }
    

     function getRatingAttribute() {

        $rating = User::join('event', 'event.user_id', '=', 'users.id')
                ->join('rating', 'rating.event_id', '=', 'event.id')
                ->where('users.id', $this->attributes['id'])
                ->avg('rating.rating');
        
        return (float) $rating;

    }
    
    
    function getOriginalImageAttribute(){
        if(isset($this->attributes['image'])){
            return $this->attributes['image'];
        }
    }
    
    
    /**
     *
     * @return type
     */
    function getImageAttribute() {

        
        
        if(isset($this->attributes['image']) && isset($this->thumbnail)){   

           //$url = tokenRoute('eventPhotos', ['user_id' => $this->id]); 
            
           $ret = [
               'original' => route('userImage', ['user_id' => $this->attributes['id']]) . "?updated=" . strtotime($this->attributes['updated_at']),
               'thumbnail' => route('userThumbnail', ['user_id' => $this->attributes['id']]) . "?updated=" . strtotime($this->attributes['updated_at'])
           ];

        }
        else {

           $ret = [
               'original' => '',
               'thumbnail' => ''
           ];

        }

        return $ret;

    }

    /**
     *
     * @param array $args
     * @param Request $request
     * @return type
     */

    public static function getValidator(Array $args, Request $request){

        $rules = [];
        $messages = [];

        if(in_array('username', $args)){

            $rules['username'] = 'required|min:4';

            $messages['username.required'] = '10|Username is required';
            $messages['username.min'] = '16|Username too short. Minimum 4 characters.';

        }


        if(in_array('email', $args)){

            $rules['email'] = 'required|email|unique:users';

            $messages['email.unique'] = '17|Account with this email already exists';
            $messages['email.required'] = '13|Email is required';
            $messages['email.email'] = '14|Invalid email format';

        }


        if(in_array('password', $args)){

            $rules['password'] = 'required|min:8';

            $messages['password.required'] = '12|Password is required';
            $messages['password.min'] = '15|Password too shorts. Minimum 8 characters.';

        }

        $validator = Validator::make($request->all(), $rules, $messages);
        return $validator;

    }


    public function user(){

         return $this->belongsToMany('Event', 'user_has_event', 'user_id', 'event_id');

    }

}
