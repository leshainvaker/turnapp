<?php

namespace App\Http\Controllers;

use App\Auth\User;
use App\Models\UserFacebook;
use App\Models\UserTwitter;
use App\Models\UserData;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
//use Illuminate\Validation\Validator;
use Validator;
use Illuminate\Http\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Custom\Exception\AppException;
use GuzzleHttp\Exception\RequestException;
use DB;

class RegisterController extends Controller {

    /**
     *
     * @param Request $request
     * @return type
     */
    public function register(Request $request) {

        DB::beginTransaction();
        
        try {
            $response = $this->processRegister($request);
        }
        Catch (RequestException $e)
        {

            DB::rollback();
            
            $response = '';
            if($response = $e->getResponse()){
                $content = json_decode($response->getBody()->getContents());
                
                foreach($content->errors as $err){
                    $response = [
                        'errorCode' => $err->code,
                        'message' => $err->message
                    ];                    
                }
            }            
            

            if($response)
            {                
                return response($response, Response::HTTP_BAD_REQUEST);
            }
            else 
            {
                return $e;
            }

        }
        
        DB::commit();
        
        return $response;
    }


    protected function processRegister(Request $request){

        $response = '';
        $newUser = null;
        ///$userData = UserData::getInstance();
        
        
        $body = $request->getContent();



        //if (!empty($body)) {

        $input = array(json_decode($body, true));

        //var_dump($input); exit;

        if ($input) {

            foreach ($input as $val) {
                if (!empty($val)) {
                    $request->merge($val);
                }
            }
        }        
        

        if ($request->has('username') && $request->has('password') && $request->has('email'))
        {
            $newUser = $this->credentialsRegistration($request);
            $input = array(
                'grant_type'    => 'password',
            );

            $request->merge($input);
        }
        else if ($request->has('twitterToken') && $request->has('twitterSecret'))
        {

            $newUser = UserTwitter::fromApi($request);

            $input = array(
                'grant_type'    => 'twitter_token',
            );

            $request->merge($input);
        }
        elseif ($request->has('facebookAcessToken'))
        {
            $newUser = UserFacebook::fromApi($request);

            $input = array(
                'grant_type'    => 'facebook_token',
                //'username' => $newUser->email
            );

            $request->merge($input);
        }
        else {

            $return = [
                'errorCode' => 10,
                'message' => 'missing parameter'
            ];

            $response = response($return, Response::HTTP_BAD_REQUEST);
            //return response($return, Response::HTTP_BAD_REQUEST);
        }



        if ($newUser instanceof User) {

            UserData::getInstance()->setUser($newUser);

            $loginResponse = response()->json(app()->make('oauth2-server.authorizer')->issueAccessToken());

            if (Response::HTTP_OK == $loginResponse->getStatusCode()) {

                //$email = empty($newUser->email) ? '' : $newUser->email;
                
                $content = [
                    'token' => json_decode($loginResponse->getContent()),
                    'user' => $newUser->fresh()
                    //'user' => 'hello'
                    //'user'  => $newUser
                ];

                return response($content, Response::HTTP_OK);
            } else {
                $response = $loginResponse;
            }
        }
        else
        {
            $response = $newUser;
        }

        return $response;

    }

    /**
     *
     * @param Request $request
     * @return type
     */
    protected function credentialsRegistration(Request $request) {

        $args = [
            'password',
            'username',
            'email'
        ];
        
        $validator = User::getValidator($args, $request);

        if ($validator->fails()) {

            $messages = $validator->errors();

            foreach ($messages->all() as $message) {

                list($errorCode, $text) = explode('|', $message);

                $return = [
                    'errorCode' => $errorCode,
                    'message' => $text
                ];

                return response($return, Response::HTTP_BAD_REQUEST);
            }
        }

        $newuser = $request->all();
        $password = Hash::make($request->input('password'));
        $newuser['password'] = $password;

        $user = User::create($newuser);
        return $user;
    }

}
