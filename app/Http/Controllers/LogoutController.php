<?php

namespace App\Http\Controllers;

use LucaDegasperi\OAuth2Server\Authorizer;

class LogoutController extends Controller {
    
    
    public function consruct(){
        
    }
    
    public function logout(){
        //Authorizer::getChecker()->getAccessToken()->expire();
        app()->make('oauth2-server.authorizer')->getChecker()->getAccessToken()->expire();
    }
    
    
}