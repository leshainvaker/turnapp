<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use LucaDegasperi\OAuth2Server\Authorizer;
use App\Auth\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DB;

//use Davibennun\LaravelPushNotification\Facades\PushNotification;

use Pushnotification;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;





class PushNotificationController extends Controller {

	public function sendNotificatonFCM() {

		$optionBuiler = new OptionsBuilder();
		$optionBuiler->setTimeToLive(60*20);


		$notificationBuilder = new PayloadNotificationBuilder('my title');
		$notificationBuilder->setBody('Hello world notification')
		                    ->setSound('default');

		$dataBuilder = new PayloadDataBuilder();
		$dataBuilder->addData(['notification_id' => '1']);

		$option = $optionBuiler->build();
		$notification = $notificationBuilder->build();

		$data = $dataBuilder->build();

		$user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
		$user = User::find($user_id);
		$token = $user->device_token;

		$downstreamResponse = FCM::sendTo($token, $option, $notification, $data); //, $data);

	    $downstreamResponse->numberSuccess();
		$downstreamResponse->numberFailure();
		$downstreamResponse->numberModification();

		var_dump($downstreamResponse);
		//return Array - you must remove all this tokens in your database
		$tokendelete = $downstreamResponse->tokensToDelete();
		var_dump($tokendelete);
		//return Array (key : oldToken, value : new token - you must change the token in your database )
		$tokentomodify = $downstreamResponse->tokensToModify();
		var_dump($tokentomodify);
		//return Array - you should try to resend the message to the tokens in the array
		$tokentoreply = $downstreamResponse->tokensToRetry();
		var_dump($tokentoreply);

		// return Array (key:token, value:errror) - in production you should remove from your database the tokens
	}


	public function sendNotificationToDevice() {

		$user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
		$user = User::find($user_id);
		$deviceToken = $user->device_token;
		$deviceToken = '111db24975bb6c6b63214a8d268052aa0a965cc1e32110ab06a72b19074c2222';
 		$message     = 'We have successfully sent a push notification!';
		// Send the notification to the device with a token of $deviceToken
		$collection = PushNotification::app( 'place2b' )
		                              ->to( $deviceToken )
		                              ->send( $message );
		 var_dump($collection);

	}
}