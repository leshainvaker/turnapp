<?php

namespace App\Http\Controllers;

use App\Event;
use App\EventInvite;
use App\UserHasEvent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use LucaDegasperi\OAuth2Server\Authorizer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Utils\Paginator;
use App\Models\Notification;
use DB;

class EventInviteController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

    }

    //Získání seznamu pozvánek k eventu
    public function getInvite(Request $request, $id) {

        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();

        /**
         * Beru jenom pozvanky, ktere nejsou schvaleny / zamitnuty
         */
        $invitations = EventInvite::join('user_has_event', 'user_has_event.event_id', '=', 'invite.event_id')
                    ->join('event', 'event.id', '=', 'user_has_event.event_id')
                    ->where('invite.event_id', '=', $id)
                    ->where('invite.type', '=', Notification::INVITATION_RECEIVED)
                    ->where('user_has_event.user_id', '=', $user_id)
                    ->where('user_has_event.user_has_event', '=', UserHasEvent::CREATOR)
                    ->select('invite.id as id', 'invite.event_id', 'invite.user_id', 'invite.created_at');

        return Paginator::wrap($invitations, $request);
    }

//Odeslání pozvánky k určitému eventu

    public function createInvite(Request $request, $id) {
        
        //echo 'createInvite'; exit;
        
        
        //var_dump($request->getContent()); exit;
        
        $user_array = json_decode($request->getContent());
        
        //$user_array = json_decode(file_get_contents('php://input'), true);
        
        
        //var_dump( file_get_contents('php://input')); exit;
        
        //var_dump($request->all()); exit;        
        //var_dump($user_array); exit;        

        $user_id_from = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
        //check if the event is not created by me


        foreach((array) $user_array as $key => $user_id){

            /**
             * Zjistim, zdali neceka pozvanka na schvaleni, pokud ne vytvorim novou
             */
            try {
                $invitation = EventInvite::where('event_id', $id)->where('user_id', $user_id)->where('type', Notification::INVITATION_RECEIVED)->firstOrFail();;
                
                //echo 'found'; exit;
            }
            Catch (ModelNotFoundException $e){
                
                //echo 'not found'; exit;

                $data = [
                    'event_id'      => $id,
                    'user_id'       => $user_id,
                    'type'          => Notification::INVITATION_RECEIVED
                ];

                $invitationId = EventInvite::insertGetId($data);

                /**
                * vytvoreni notifikaci pro pozvane uzivatele
                */
                Notification::add($invitationId, Notification::INVITATION_RECEIVED, $user_id, $user_id_from);

            }

        }
        
        
        /**
         * Pokud presahnu pozvankami pocet mist na eventu, zvecim toto cislo o pocet pozvanek, ktere jsou navic
         */
        $event = Event::where('id', $id)->first();
        $currentVisitorCount = UserHasEvent::where('event_id', $id)->where('user_has_event', 0)->count();        
        $waitingInviteCount = EventInvite::where('event_id', $id)->where('type', Notification::INVITATION_RECEIVED)->count();
        
        $neededSpace = $waitingInviteCount + $currentVisitorCount;
        
        
        if($neededSpace > $event->capacity){
            $event->capacity = $neededSpace;
            $event->save();
        }
        
        
        
        return response('', Response::HTTP_NO_CONTENT);

    }


    /**
     * Zruseni pozvanky
     *
     * @param Request $request
     * @param type $event_id
     * @param type $invite_id
     * @return type
     */
    public function deleteInvite(Request $request, $event_id, $invite_id) {

        try {
            $invitation = EventInvite::where('event_id', $event_id)->where('id', $invite_id)->firstOrFail();
            $invitation->delete();
        }
        Catch (ModelNotFoundException $e) {

            /*
            $message = [
                'errorCode' => 80,
                'message' => 'Invitation not found'
            ];
            */

            //return response($message, Response::HTTP_NOT_FOUND);
        }

        return response('', Response::HTTP_NO_CONTENT);

    }



    public function updateInvite(Request $request, $event_id, $request_id){

        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
        $request_action = $request->input('action');

        try {

            $invitation = EventInvite::where('id', $request_id)->where('event_id', $event_id)->firstOrFail();

            switch ($request_action) {

                case "refuse":
                    //$invite_state = EventInvite::DECLINED;
                    $notification_type = Notification::INVITATION_DECLINED;
                    break;

                case "accept":
                    //$invite_state = EventInvite::ACCEPTED;
                    $notification_type = Notification::INVITATION_ACCEPTED;

                    // uzivatele, ktery prijal pozvanku priradim k eventu
                    /*
                    $userHasEvent                   = new UserHasEvent();
                    $userHasEvent->event_id         = $event_id;
                    $userHasEvent->user_id          = $user_id;
                    $userHasEvent->user_has_event   = 0;
                    $userHasEvent->firstOrCreate();
                    */

                    UserHasEvent::firstOrCreate([
                        'event_id'          => $event_id,
                        'user_id'           => $user_id,
                    ], [
                        'user_has_event'    => 0
                    ]);

                    break;

                default:
                    return response('', Response::HTTP_BAD_REQUEST);
            }


            // aktualizace stavu pozvanky
            $invitation->type = $notification_type;
            $invitation->save();

            /**
             * vytvoreni notifikaci autora pozvanky
             */
            /*
            $event =    DB::table('users')
                        ->join('user_has_event', 'user_has_event.user_id', '=', 'users.id')
                        ->join('event', 'event.id', '=', 'user_has_event.event_id')
                        ->select('user_has_event.user_id', 'event.capacity')
                        ->where('event.id', '=', $event_id)
                        ->where('user_has_event.user_has_event', '=', UserHasEvent::CREATOR)
                        ->first();
            */

            $event = Event::where('id', $event_id)->first();
            
            Notification::add($invitation->id, $notification_type, $event->user_id, $invitation->user_id);

            return response('', Response::HTTP_NO_CONTENT);

        } Catch (ModelNotFoundException $e){

            $message = [
                'errorCode' => 80,
                'message' => 'Invitation not found'
            ];

            return response($message, Response::HTTP_NOT_FOUND);

        }
    }


    /*
      public function updateInvite(Request $request, $event_id, $request_id) {

      $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
      $request_action = $request['action'];

      switch ($request_action) {
      case "refuse":
      $action = 'REQUEST_DECLINED';
      break;
      case "accept":
      $action = 'REQUEST_ACCEPTED';
      break;
      default:
      return response('', Response::HTTP_BAD_REQUEST);
      }

      EventRequest::where('event_id', $event_id)->where('id', $request_id)->update(['type' => $action]);
      return response('', Response::HTTP_NO_CONTENT);
      }
     */
}
