<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use LucaDegasperi\OAuth2Server\Authorizer;
use App\Auth\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DB;

//use Davibennun\LaravelPushNotification\Facades\PushNotification;

use Pushnotification;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;





class PushNotificationController extends Controller {

	public function sendNotificatonFCM() {


		$token = 'elhIgC1iycI:APA91bEEZfOBU196Ig0wWoTDvSaTSM-LWtmZlXN3VLCkzObKeSHYjx6QSn_p47qxlnf4RpLeubYdIbNC4ZkdYqTn-QR2KE0gFE8fNDmxL7DgWKvlvxeX9hrZggF5r-9huajDSYbodlzH';

		$url = 'https://gcm-http.googleapis.com/gcm/send';
		$fields = array(
			'registration_ids' =>  array (
				$token
			),
			'notification' => array(
				'title' => 'nazev appky',
                'body' => 'Podívejte se na naše nově doporučené nabídky.'
          )
      );
      $headers = array(
          'Authorization: key=AIzaSyADg0IgWm7NhUzl1XhFICdhachJLzGlaHU',
          'Content-Type: application/json'
      );

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

      $result = curl_exec($ch);



      if ($result === FALSE) {
          die('Sending notification with Curl failed: ' . curl_error($ch));
      }


      curl_close($ch);

		echo $result;

		$user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
		$user = User::find($user_id);
		$token = 'elhIgC1iycI:APA91bEEZfOBU196Ig0wWoTDvSaTSM-LWtmZlXN3VLCkzObKeSHYjx6QSn_p47qxlnf4RpLeubYdIbNC4ZkdYqTn-QR2KE0gFE8fNDmxL7DgWKvlvxeX9hrZggF5r-9huajDSYbodlzH';
		$message = "Hello world";

		$url = 'https://fcm.googleapis.com/fcm/send';


		$fields = array (
			'registration_ids' => array (
				$token
			),
			'data' => array (
				"message" => $message
			)
		);

		$fields = json_encode ( $fields );

		$headers = array (
			'Authorization: key='. "AIzaSyADg0IgWm7NhUzl1XhFICdhachJLzGlaHU",
			'Content-Type: application/json'
		);

		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

		$result = curl_exec ( $ch );
		echo $result;

		curl_close ( $ch );


		$optionBuiler = new OptionsBuilder();
		$optionBuiler->setTimeToLive(60*20);

		$notificationBuilder = new PayloadNotificationBuilder('my title');
		$notificationBuilder->setBody('Hello world notification')
		                    ->setSound('default');

		/*
		$dataBuilder = new PayloadDataBuilder();
		$dataBuilder->addData(['a_data' => 'my_data']);
		*/

		$option = $optionBuiler->build();
		$notification = $notificationBuilder->build();

		/*$data = $dataBuilder->build();*/

		$user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
		$user = User::find($user_id);
		$token = $user->device_token;

		//$token = "fb3MOBxACkE:APA91bHjyJwkcnZqnqEO4T_dHPPNIKfbMskcQgO7cc8-VvR-8V9c6tWUmw5pAGRQFEdLpRMWdqQYVx1W5l9tKMHY8eVbTueP8Yz1SzSQPDnrgjtaSc-l5w-OdcJL9XCQ-FbZD8We_L0Q";

		$downstreamResponse = FCM::sendTo($token, $option, $notification); //, $data);

	    $downstreamResponse->numberSuccess();
		$downstreamResponse->numberFailure();
		$downstreamResponse->numberModification();

		var_dump($downstreamResponse);

		//return Array - you must remove all this tokens in your database
		$tokendelete = $downstreamResponse->tokensToDelete();
		var_dump($tokendelete);
		//return Array (key : oldToken, value : new token - you must change the token in your database )
		$tokentomodify = $downstreamResponse->tokensToModify();
		var_dump($tokentomodify);

		//return Array - you should try to resend the message to the tokens in the array
		$tokentoreply = $downstreamResponse->tokensToRetry();
		var_dump($tokentoreply);

		// return Array (key:token, value:errror) - in production you should remove from your database the tokens

	}


	public function sendNotificationToDevice() {

		$user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
		$user = User::find($user_id);
		$deviceToken = $user->device_token;
		$deviceToken = '111db24975bb6c6b63214a8d268052aa0a965cc1e32110ab06a72b19074c2222';
 		$message     = 'We have successfully sent a push notification!';
		// Send the notification to the device with a token of $deviceToken
		$collection = PushNotification::app( 'place2b' )
		                              ->to( $deviceToken )
		                              ->send( $message );
		 var_dump($collection);

	}
}