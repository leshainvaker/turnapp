<?php

namespace App\Http\Controllers;

use Illuminate\Auth\AuthManager;
use LucaDegasperi\OAuth2Server\Authorizer;
use LucaDegasperi\OAuth2Server\Storage\FluentRefreshToken;

class TokenController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function token(Authorizer $authorizer){

        $authorizer->validateAccessToken();
        $issuer = $authorizer->getIssuer();
        $tokenType = $issuer->getTokenType();

        $checker = $authorizer->getChecker();
        $token = $checker->getAccessToken();

        $token_id = $token->getId();

        //$refreshTokenStorage = new FluentRefreshToken(app()['db']);

        $result = app()['db']->table('oauth_access_tokens')                
                ->where('id', $token_id)
                ->first();

        if (is_null($result)) {
            return null;
        }

        $tokenType->setParam('access_token', $result->id);
        $tokenType->setParam('expires_in', (int) $result->expire_time);
        $tokenType->setParam('refresh_token', $result->id);
        
        return $tokenType->generateResponse();
    }

}
