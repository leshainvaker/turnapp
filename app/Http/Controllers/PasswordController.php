<?php

namespace App\Http\Controllers;

use App\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Auth\AuthManager;
use LucaDegasperi\OAuth2Server\Authorizer;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Illuminate\Support\Facades\Hash;

use Mail;

class PasswordController extends Controller {
    
    
    public function construct(Request $request){
        
        
        
    }
    
    
    public function recovery(Request $request){        
        
        
        //email
        $email = $request->input('email');
        
        if(!$email){
            
            $message = [
                'errorCode' => 60,
                'message' => 'Missing email'
            ];

            return response($message, Response::HTTP_BAD_REQUEST);            
        }
        
        
        
        try 
        {
            $user = User::where('email', $email)->firstOrFail();
        }
        Catch (ModelNotFoundException $e)
        {

            $message = [
                'errorCode' => 61,
                'message' => 'Account with this email does not exists'
            ];

            return response($message, Response::HTTP_NOT_FOUND);
        }
        
        $password   = str_random(8);
        $hash       = Hash::make($password);
        
        $user->password = $hash;
        $user->save();
        
        $text = 'New password ' . $password;
        
        $result = Mail::raw($text, function($msg) use ($user) { 
            $msg->to([$user->email]); 
            $msg->from(['turn_app@web-4-all.cz']);
            $msg->subject('Password recovery');
        });
        
        return response('', Response::HTTP_NO_CONTENT);
        
    }
    
    
}