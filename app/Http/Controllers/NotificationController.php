<?php

namespace App\Http\Controllers;

use Illuminate\Auth\AuthManager;
use LucaDegasperi\OAuth2Server\Authorizer;

use Illuminate\Http\Request;
use App\Models\Notification;
use App\Utils\Paginator;

class NotificationController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function all(Request $request){

        // current user id
        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();

        //$notifications = Notification::all();
        $notifications = Notification::where('user_id_to', $user_id)->orderBy('created_at', 'desc');

        //var_dump(get_class($notifications)); exit;

        //var_dump($notifications->first()); exit;


        $paginator = Paginator::wrap($notifications, $request);

        //var_dump($paginator); exit;

        return $paginator;
    }
    
    public function read(Request $request, $notification_id){
        
        try {
            
            $data = array(
                'active' => 0
            );            
            
            $notification = Notification::where('id', $notification_id)->update($data);
            //$notification->active = 0;
            return Notification::where('id', $notification_id)->firstOrFail();
            
            
            
            //$notification->update($data);
            //$notification->save();
        }
        Catch (ModelNotFoundException $e){

        }         
    }


    public function three_hours_before(Request $request) {


    }
}