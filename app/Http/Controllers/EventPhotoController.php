<?php

namespace App\Http\Controllers;

use Illuminate\Auth\AuthManager;
use LucaDegasperi\OAuth2Server\Authorizer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Utils\Paginator;
use App\Models\EventPhoto;
use Image;

class EventPhotoController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * vlozeni obrazku
     *
     * @param Request $request
     * @return type
     */
    public function add(Request $request, $event_id) {

        if ($request->has('image')) {

            try {

                $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();

                $img = Image::make($request->input('image'));

                //fit a 300 300

                $data['original'] = (string) $img->encode("data-url");

                $thumb = Image::make($request->input('image'));
                $thumb->fit(300, 300);

                $data['user_id'] = $user_id;

                $data['thumbnail'] = (string) $thumb->encode("data-url");
                $data['event_id'] = $event_id;

                EventPhoto::create($data);
            } Catch (\Exception $e) {

                $message = [
                    'errorCode' => 74,
                    'message' => 'Upload failed'
                ];

                return response($message, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     *
     * @param Request $request
     * @return type
     */
    public function get(Request $request, $event_id) {

        return Paginator::wrap(EventPhoto::where('event_id', $event_id), $request);
    }

    /**
     *
     * @param Request $request
     * @return type
     */
    public function delete(Request $request, $event_id, $image_id) {

        EventPhoto::where('event_id', $event_id)->where('id', $image_id)->delete();

        return response('', Response::HTTP_NO_CONTENT);
    }

    public function image($image_id) {

        //var_dump($user_id); exit;

        try {
            //$user = User::find($user_id)->firstOrFail();
            $photo = EventPhoto::where('id', $image_id)->first();


            if (!empty($photo->originalImage)) {

                $img = Image::make($photo->originalImage);
                echo $img->response('jpg', 70);
            }
        } Catch (ModelNotFoundException $e) {

        }
    }

    public function thumbnail($image_id) {

        try {

            $photo = EventPhoto::where('id', $image_id)->first();

            if (!empty($photo->thumbnailImage)) {
                $img = Image::make($photo->thumbnailImage);
                echo $img->response('jpg', 70);
            }
        } Catch (ModelNotFoundException $e) {

        }
    }

}
