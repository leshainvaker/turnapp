<?php

namespace App\Http\Controllers;

use App\EventRequest;
use App\UserHasEvent;
use App\Models\Notification;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use LucaDegasperi\OAuth2Server\Authorizer;
use App\Utils\Paginator;
use DB;

class EventRequestController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        
    }

    
    /**
     * Vrati pouze requesty, ktere nejsou zamitnuty / schvaleny
     * 
     * @param Request $request
     * @param type $event_id
     * @return type
     */
    public function getRequest(Request $request, $event_id) {

        //return Paginator::wrap(DB::table('note'), $request);
        //Je možné pouze u eventu, který jsem sám vytvořil
        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();

        //return Paginator::wrap(DB::table('event_id'), $request);
        //$EventRequest = EventRequest::where('event_id', (int) $id)->get();

        $eventRequest = EventRequest::join('user_has_event', 'user_has_event.event_id', '=', 'request.event_id')
                ->select('request.id', 'request.event_id', 'request.user_id', 'request.created_at')
                ->where('user_has_event.event_id', $event_id)
                ->where('user_has_event.user_id', $user_id)
                ->where('user_has_event.user_has_event', UserHasEvent::CREATOR)
                ->where('request.type', Notification::REQUEST_RECEIVED);


        return Paginator::wrap($eventRequest, $request);
    }

    /**
     *
     *
     * @param Request $request
     * @param type $event_id
     * @return type
     */
    public function createRequest(Request $request, $event_id) {

        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();

        /*
          $add_input = array(
          'event_id' => $id,
          'user_id' => $user_id,
          'type' => 'REQUEST_RECEIVED',
          );

          $request->merge($add_input);

          //updateOrCreate
          $EventRequest = EventRequest::create(
          $request->all()
          ); */



        try {

            /**
             * zjistim, zda li neexistuje cekajici request na schvaleni, pokud ne vytvorim novy
             */
            try {
                EventRequest::where('user_id', $user_id)->where('event_id', $event_id)->where('type', Notification::REQUEST_RECEIVED)->firstOrFail();
            } Catch (ModelNotFoundException $e) {

                // najdu autora eventu
                $event = \App\Auth\User::join('user_has_event', 'user_has_event.user_id', '=', 'users.id')
                        ->join('event', 'event.id', '=', 'user_has_event.event_id')
                        ->select('user_has_event.user_id', 'event.capacity', 'event.private')
                        ->where('event.id', '=', $event_id)
                        ->where('user_has_event.user_has_event', '=', UserHasEvent::CREATOR)
                        ->firstOrFail();

               if ($event->private = 1) {
                
                $data = [
                    'event_id' => $event_id,
                    'user_id' => $user_id, // uzivatel, ktery udelal request
                    'type' => Notification::REQUEST_RECEIVED
                ];


                $eventRequest = EventRequest::create($data);

                /**
                 * vytvorim notifikaci pro majitele eventu
                 */
                Notification::add($eventRequest->id, Notification::REQUEST_RECEIVED, $event->user_id, $user_id);
               } else {
                   $data = [
                       'event_id' => $event_id,
                       'user_id' => $user_id, // uzivatel, ktery udelal request
                       'type' => Notification::REQUEST_ACCEPTED
                   ];


                   $eventRequest = EventRequest::create($data);

                   /**
                    * vytvorim notifikaci pro majitele eventu
                    */
                   Notification::add($eventRequest->id, Notification::REQUEST_ACCEPTED, $event->user_id, $user_id);
               }
            }
        }         
        Catch (ModelNotFoundException $e) {
            /**
             * Nepodarilo se najit eventu, respektive autora eventu
             */            
            $message = [
                'errorCode' => 70,
                'message' => 'Requested event not found'
            ];

            return response($message, Response::HTTP_NOT_FOUND);
        }

        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * Zruseni requestu uzivatelem, ktery request odeslal
     * 
     * @param Request $request
     * @param type $event_id
     * @param type $request_id
     * @return type
     */
    public function deleteRequest(Request $request, $event_id, $request_id) {

        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
        EventRequest::where('event_id', $event_id)->where('id', $request_id)->where('user_id', $user_id)->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * Schvaleni / zamitnuti requestu
     * 
     * @param Request $request
     * @param type $event_id
     * @param type $request_id
     * @return type
     */
    public function updateRequest(Request $request, $event_id, $request_id) {

        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
        $request_action = $request['action'];

        try {

            /**
             * Schvalit / zamitnout mohu pouze request cekajici na schvaleni / zamitnuti
             */
            $eventRequest = EventRequest::where('id', $request_id)->where('type', Notification::REQUEST_RECEIVED)->firstOrFail();

            switch ($request_action) {
                case "refuse":
                    $action = 'REQUEST_DECLINED';
                    break;
                case "accept":

                    $action = 'REQUEST_ACCEPTED';

                    /**
                     * Priradim uzivatele, ktery odeslal request k eventu
                     */
                    /*
                      $userHasEvent                   = new UserHasEvent();
                      $userHasEvent->event_id         = $event_id;
                      $userHasEvent->user_id          = $eventRequest->user_id;
                      $userHasEvent->user_has_event   = 0;
                      $userHasEvent->firstOrCreate();
                     */

                    $data = array(
                        'event_id' => $event_id,
                        'user_id' => $eventRequest->user_id,
                        'user_has_event' => 0
                    );

                    UserHasEvent::firstOrCreate($data);

                    break;
                default:
                    
                    $message = [
                        'errorCode' => 91,
                        'message' => 'Invalid request action'
                    ];
                    
                    return response($message, Response::HTTP_BAD_REQUEST);
            }


            // vytvorim notifikaci pro uzivatele, ktery poslal request na eventu
            Notification::add($eventRequest->id, $action, $eventRequest->user_id, $user_id);


            //
            EventRequest::where('event_id', $event_id)
                    ->where('id', $request_id)
                    ->update([
                        'type' => $action
            ]);

            return response('', Response::HTTP_NO_CONTENT);
            
        } Catch (ModelNotFoundException $e) {

            $message = [
                'errorCode' => 90,
                'message' => 'Request not found'
            ];

            return response($message, Response::HTTP_NOT_FOUND);
        }
    }
}    