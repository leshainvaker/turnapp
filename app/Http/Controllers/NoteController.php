<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use Illuminate\Auth\AuthManager;
use LucaDegasperi\OAuth2Server\Authorizer;
use App\Note;
use Illuminate\Http\Request;
use App\Utils\Paginator;
use DB;

class NoteController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

    }
    public function getNote(Request $request, $id) {

        //vracet note jen pro konkretni event

        $note = Note::where('event_id', '=', $id);
        return Paginator::wrap($note, $request);

    }

    /**
     * Vytvoreni poznamky k eventu
     *
     * @param Request $request
     * @param type $event_id
     */
    public function addNote(Request $request, $event_id){

        //if($request->has('note')){

        $note               = new Note();
        $note->event_id     = $event_id;
        $note->note_title   = $request->input('note_title','');
        $note->note         = $request->input('note', '');

        $note->save();
        $note->fresh();

        /**
        * notifikace o pridani note
        */
        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
        Notification::eventNotification($note->event_id, Notification::NOTE_ADDED, $user_id);

        return $note;

    }

    public function editNote(Request $request, $note_id ){

        $note = Note::find($note_id);
        $note->note_title   = $request->input('note_title','');
        $note->note         = $request->input('note', '');
        $note->save();
        $note->fresh();
        return $note;

    }

    public function deleteNote(Request $request, $note_id ){

        Note::destroy($note_id);
        return response()->json('deleted');

    }
}