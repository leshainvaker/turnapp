<?php
namespace App\Http\Controllers;

use Illuminate\Auth\AuthManager;
use LucaDegasperi\OAuth2Server\Authorizer;
use App\Event;
use Illuminate\Http\Request;
use App\Utils\Paginator;
use App\Models\EventPhoto;
use Image;


class AdminEventController extends AdminController
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('adminAccess');
        //return redirect('/');        
        //return parent::__construct();

    }

    public function eventList(Request $request){

        $paginator = Paginator::wrap(Event::query()->orderBy('id', 'desc')->withTrashed(), $request);

        return view('events')->with( ['paginator' => $paginator]);
    }
    
    public function filter(Request $request){        
        $search = isset($_GET['search']) ? $_GET['search'] : '';
        $paginator = Paginator::wrap(Event::where('name', 'LIKE', '%' . $search . '%')->orderBy('id', 'desc')->withTrashed(), $request);
        return view('events')->with( ['paginator' => $paginator]);
    }    
    
    

    public function edit(Request $request, $event_id) {

        $event = Event::where('id', '=', $event_id)->withTrashed()->first();

        return view('event-edit', ['event' => $event]);
    }


    public function save(Request $request, $event_id) {        

        /**
         * 
         */
        //$response = app('App\Http\Controllers\EventController')->updateEvent($request, $event_id);
        
        $event = Event::withTrashed()->find($event_id);
        $response = Event::updateEvent($request, $event);     
        
        if(200 != $response->status()){

            $errorMessages = array();
            
            $responseContent = \json_decode($response->content());
            $errorMessages[] = $responseContent->message;
            
            return view('event-edit', ['event' => $event, 'errorMessages' => $errorMessages ]);
            
        }
        
        $url = tokenRoute('eventEdit', ['event_id' => $event_id]);
        return redirect($url);
    }


    public function delete(Request $request, $event_id) {

        $event = Event::where('id', $event_id);
        $event->delete();

        $url = tokenRoute('eventList');
        return redirect($url);
        //return view('users')->with(['paginator' => $paginator]);
    }


    /**
     *
     * @param Request $request
     * @param type $event_id
     * @return type
     */
    public function restore(Request $request, $event_id) {

        $event = Event::where('id', $event_id);
        $event->restore();

        $url = tokenRoute('eventList');
        return redirect($url);
    }

    
    
    public function photos(Request $request, $event_id){
        
        $event = Event::where('id', $event_id)->withTrashed()->first();
        
        $photos = EventPhoto::where('event_id', $event_id)->orderBy('id', 'DESC')->get();
        return view('event-photos')->with(['photos' => $photos, 'event' => $event]);
        //return 'hello';
    }
    
    
    
    public function deleteImage(Request $request, $event_id, $image_id){  
        
        $image = EventPhoto::where('id', $image_id);
        $image->delete();
        
        $url = tokenRoute('eventPhotos', ['event_id' => $event_id]);
        return redirect($url);
    }
    
    public function saveImage(Request $request, $event_id){
        
        
        
        //if(isset($_FILES['image']['tmp_name']) && is_array($_FILES['image']['tmp_name'])){

            foreach ($_FILES['image']['tmp_name'] as $tmp_name){            
                
                $data = [];
                
                try {                    
                    
                    $img                    = Image::make($tmp_name);                    
                    $data['original']       = $img->encode('jpg')->encode("data-url");   
                    
                    
                    $thumb                  = Image::make($tmp_name);                    
                    $thumb->fit(300, 300);
                    $data['thumbnail']      = $thumb->encode('jpg')->encode("data-url");
                    $data['event_id']       = $event_id;                    
                    
                    EventPhoto::create($data);                    

                } Catch (\Exception $e) {
                    
                    
                    
                    $messages[] = 'Image upload failed';
                }                
            }
        //}

        
        $url = tokenRoute('eventPhotos', ['event_id' => $event_id]);
        return redirect($url);
        
    }
    
    //
}
