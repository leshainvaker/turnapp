<?php

namespace App\Http\Controllers;

use Illuminate\Auth\AuthManager;
use LucaDegasperi\OAuth2Server\Authorizer;
use App\Auth\User;
use Illuminate\Http\Request;
use App\Utils\Paginator;
use App\Models\School;
use Illuminate\Support\Facades\Hash;

use Image;

class AdminUserController extends AdminController {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //return parent::__construct();
        $this->middleware('adminAccess');
    }

    public function userList(Request $request) {

        $users = User::leftJoin('school', 'school.id', '=', 'users.school')
                ->orderBy('users.username', 'asc')
                ->select('users.*', 'school.name as school_name');

        $paginator = Paginator::wrap($users, $request);

        return view('users')->with(['paginator' => $paginator,]);
    }
    
    public function filter(Request $request) {

        $search = isset($_GET['search']) ? $_GET['search'] : '';
        
        $users = User::leftJoin('school', 'school.id', '=', 'users.school')
                ->orderBy('users.username', 'asc')
                ->select('users.*', 'school.name as school_name')
                ->where('username', 'LIKE', '%' . $search  . '%');

        $paginator = Paginator::wrap($users, $request);

        return view('users')->with(['paginator' => $paginator]);
    }    
    

    

    public function edit(Request $request, $user_id) {

        $schools = School::orderBy('name', 'ASC')->get();

        $user = User::where('id', $user_id)->first();
        return view('user-edit', ['user' => $user, 'schools' => $schools]);
    }

    public function save(Request $request, $user_id) {

        $messages = [];

        /**
         * pole dat, ktere se budou upgradovat
         */
        $data = array();

        $user = User::where('id', $user_id)->first();


        if ($request->has('image-filename')) {

            try {
                $img = Image::make($_FILES['image']['tmp_name']);
                $data['image'] = $img->encode('jpg')->encode("data-url");
                
                $thumb = Image::make($_FILES['image']['tmp_name']);
                $thumb->fit(300, 300);
                
                $data['thumbnail'] = $thumb->encode('jpg')->encode("data-url");
                
            } Catch (\Exception $e) {
                $messages[] = 'Image upload failed';
            }
        }



        $validate = [
            'username',
                //'email'
        ];

        $inputs = [
            'username',
            'description',
            'birthday',
            'school',
        ];


        if ($user->email != $request->input('email')) {
            $validate[] = 'email';
            $inputs[] = 'email';
        }


        if (!empty($request->input('password'))) {
            $validate[] = 'password';
            $inputs[] = 'password';
            //$request->input('password') = Hash::make($request->input('password'));
        }


        $validator = User::getValidator($validate, $request);

        if ($validator->fails()) {

            foreach ($validator->errors()->all() as $err) {
                list($errorCode, $text) = explode('|', $err);
                $messages[] = $text;
            }

            return view('user-edit', ['errorMessages' => $messages] );
        }




        foreach ($inputs as $key) {
            //if (!empty($request->input($key))) {

                if ('password' == $key && !empty($request->input('password'))) {
                    $data[$key] = Hash::make($request->input('password'));
                } else {
                    $data[$key] = $request->input($key, '');
                }
            ///}
        }


        $user->update($data);
        $user->save();

        $url = tokenRoute('userEdit', ['user_id' => $user_id]);
        return redirect($url);
    }

    public function delete(Request $request, $user_id) {

        $user = User::where('id', $user_id);
        $user->delete();        
        
        $url = tokenRoute('userList');
        return redirect($url);
    }


    public function deleteImage(Request $request, $user_id){

        $user = User::where('id', $user_id)->first();
        $user->image = '';
        $user->thumbnail = '';
        $user->save();
        
        $url = tokenRoute('userEdit', ['user_id' => $user_id]);
        return redirect($url);
    }
}
