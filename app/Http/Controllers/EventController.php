<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Event;
use App\Pin;
use App\UserHasEvent;
use App\Rate;
use App\Models\EventPhoto;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use LucaDegasperi\OAuth2Server\Authorizer;
use App\Utils\Paginator;
use App\Auth\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DB;

class EventController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

    }

    public function index() {
        $Event = 'test';
        return 'test';
    }

    public function isInvited($event_id, $user_id) {

        $is_invited = Event::join('invite', 'event.id', '=', 'invite.event_id')
                ->where('invite.user_id', '=', $user_id)
                ->where('invite.event_id', '=', $event_id)
                ->first();
        return $is_invited;
    }

    public function requestAccepted($event_id, $user_id) {

        $request_accepted = Event::join('request', 'event.id', '=', 'request.event_id')
                ->where('request.user_id', '=', $user_id)
                ->where('request.event_id', '=', $event_id)
                ->where('request.type', '=', 'REQUEST_ACCEPTED')
                ->first();

        return $request_accepted;
    }

    /**
     * @param $id
     * @return
     */
    public function getEvent($id) {

        /*
         * $Event = Event::find($id);
          //check zda na event uzivatel prihlasen - ma pravo videt destinaci
          if ($datetime_now < $Event->date) {
          }
          return response()->json($Event);
         */

        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
        $datetime_now = (new \DateTime())->format('Y-m-d H:i:s');

        $Event = Event::withTrashed()->where('id', '=', $id)->first();

        //event ktery muzu videt

        if (($Event->user_id != (int) $user_id) && (null == $this->isInvited($id, $user_id)) && (null == $this->requestAccepted($id, $user_id))) {

            unset($Event['gps']);
            unset($Event['city']);
            unset($Event['street']);
            unset($Event['zip']);
        }

        return $Event;
    }

    /**
     * @param Request $request
     * @return
     */
    public function createEvent(Request $request) {


        //add some validation
        //asi bych mel validatovat vsechno

        /* $validate = $this->validate( $request, [
          //minimalne jedno misto na party
          'capacity' => 'required',
          'street'   => 'required',
          'number'   => 'required',
          'city'     => 'required',
          'zip'      => 'required',
          ]);
          var_dump($validate);
         */


        $response = Event::getCoordinates($request['street'] . ' + ' . $request['number'] . ' + ' . $request['city'] . ' + ' . $request['zip']);

        if (isset($response['errorCode'])) {
            switch ($response['errorCode']) {
                case 101:
                    return response($response, Response::HTTP_FORBIDDEN);
                    break;
                case 102:
                    return response($response, Response::HTTP_FORBIDDEN);
                    break;
                default:
            }
        }

        //dej mi aktualniho usera

        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();

        $Event = new Event();

        $Event->name = $request['name'];
        $Event->date = $request['date'];
        $Event->capacity = $request['capacity'];
        $Event->indoor = boolval($request['indoor']);
        $Event->apartament = $request['apartament'];
        $Event->street = $request['street'];
        $Event->city = $request['city'];
        $Event->number = $request['number'];
        $Event->zip = $request['zip'];
        $Event->private = boolval($request['private']);
        $Event->school_id = $request['school_id'];
        $Event->description = $request['description'];
        $Event->gps = $response[0] . ' ' . $response[1];
        $Event->user_id = $user_id;
        $Event->save();
        $Event->fresh();

        $User_Has_Event = new UserHasEvent();
        $User_Has_Event->event_id = $Event->id;
        $User_Has_Event->user_id = $user_id;
        $User_Has_Event->user_has_event = 1;
        $User_Has_Event->save();

        //$Pins = Pin::all();
        $lat_lon = $response[0] . ',' . $response[1];
        $pluck_pin = Event::distance(500, $lat_lon)->pluck('pin_id');
        //zkusime jestli neexistuje pin blizko naseho bodu

        if (NULL != $pluck_pin) {
            $Event->pin_id = $pluck_pin;
            $Event->save();
            //zrejme ne tak ulozime novy
        } else {
            $response = Event::randDistance($response[0], $response[1], 500);
            $Pin = new Pin();
            $Pin->gps = $response[0] . ' ' . $response[1];
            $Pin->save();
            $Event->pin_id = $Pin->id;
            $Event->save();
        }

        $Event->fresh();

        return response()->json($Event);
    }

    public function deleteEvent(Request $request, $id) {



        //smazani zaznamu eventu - tady je otazka co se ma vse mazat - a jestli vubec neco - spis nic
        // notifikace
        DB::beginTransaction();
        try {

            $Event = Event::where('id', $id)->first()->delete();
        } catch (ModelNotFoundException $e) {
            //return response()->json('deleted');
        } catch (\Exception $e) {
            DB::rollback();
            $message = [
                'errorCode' => 65,
                'message' => 'Delete failed'
            ];
            return response($message, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        DB::commit();
        //$user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
        return response()->json('deleted');
    }

    //update event
    public function updateEvent(Request $request, $id) {
        $Event = Event::find($id);
        return Event::updateEvent($request, $Event);
    }

    public function getResponseArray($response) {
        return json_decode($response->getBody()->getContents(), true);
    }

    //Ziskani eventu ktere navstivim
    public function getEventAttending(Request $request) {

        if (!isset($_GET['type'])) {

            $message = [
                'errorCode' => 51,
                'message' => 'Missing search parameter attending event'
            ];

            return response($message, Response::HTTP_BAD_REQUEST);
        }

        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
        $datetime_now = (new \DateTime())->format('Y-m-d H:i:s');

        switch ($_GET['type']) {
            case 'incoming':
                $Event = $this->incomingEvents($datetime_now, $user_id, $request);
                break;
            case 'past':
                $Event = $this->pastEvent($datetime_now, $user_id, $request);
                break;
            case 'pending':
                $Event = $this->pendingEvent($datetime_now, $user_id, $request);
                break;
            case 'unconfirmed':
                $Event = $this->unconfirmedEvent($datetime_now, $user_id, $request);
                break;
            default :
                $message = [
                    'errorCode' => 52,
                    'message' => 'Bad search parameter attending event'
                ];
                return response($message, Response::HTTP_BAD_REQUEST);
                break;
        }

        return $Event;
        //return Paginator::wrap()
        //return Paginator::wrap(Event::query(), $request);
    }

    public function getEventFromUser(Request $request, $user_id) {

        $events = Paginator::wrap(Event::where( 'user_id', '=', $user_id ), $request);
        return $events;

    }

    public function getEventOrganizing(Request $request) {

        if (!isset($_GET['type'])) {

            $message = [
                'errorCode' => 61,
                'message' => 'Missing search parameter organizing event'
            ];

            return response($message, Response::HTTP_BAD_REQUEST);
        } elseif ('incoming' == $_GET['type']) {
            $operator = '>';
        } elseif ('past' == $_GET['type']) {
            $operator = '<';
        } else {
            $message = [
                'errorCode' => 62,
                'message' => 'Bad search parameter organizing event'
            ];
            return response($message, Response::HTTP_BAD_REQUEST);
        }

        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
        $datetime_now = (new \DateTime())->format('Y-m-d H:i:s');

        $event = Paginator::wrap(Event::withTrashed()
                                ->where('event.date', $operator, $datetime_now)
                                ->where('event.user_id', '=', $user_id)->orderBy('event.date', 'asc'), $request);

        return response()->json($event);
    }

    public function incomingEvents($datetime_now, $user_id, $request) {

        $event = Paginator::wrap(Event::withTrashed()
                                ->leftJoin('request', 'event.id', '=', 'request.event_id')
                                ->leftJoin('invite', 'event.id', '=', 'invite.event_id')
                                ->where('event.date', '>', $datetime_now)
                                ->where(function($query) {
                                    $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
                                    return $query
                                            ->where('request.user_id', '=', $user_id)
                                            ->where('request.type', '=', 'REQUEST_ACCEPTED')
                                            ->orWhere(function($query) {
                                                $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
                                                $query->where('invite.user_id', '=', $user_id)
                                                ->where('invite.type', '=', 'INVITE_ACCEPTED');
                                            });
                                })->distinct(), $request);
        return $event;
    }

    public function pastEvent($datetime_now, $user_id, $request) {

        $event = Paginator::wrap(Event::withTrashed()
                                ->leftJoin('request', 'event.id', '=', 'request.event_id')
                                ->leftJoin('invite', 'event.id', '=', 'invite.event_id')
                                ->where('event.date', '<', $datetime_now)
                                ->where(function($query) {
                                    $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
                                    return $query
                                            ->where('request.user_id', '=', $user_id)
                                            ->where('request.type', '=', 'REQUEST_ACCEPTED')
                                            ->orWhere(function($query) {
                                                $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
                                                $query->where('invite.user_id', '=', $user_id)
                                                ->where('invite.type', '=', 'INVITE_ACCEPTED');
                                            });
                                })->orderBy('event.date', 'desc')->distinct(), $request);

        return $event;
    }

    public function pendingEvent($datetime_now, $user_id, $request) {


        $event = Paginator::wrap(Event::withTrashed()
                                ->join('request', 'event.id', '=', 'request.event_id')
                                ->where('event.date', '>', $datetime_now)
                                ->where('request.user_id', '=', $user_id)
                                ->where('request.type', '=', 'REQUEST_RECEIVED'), $request);
        unset($event['gps']);
        unset($event['city']);
        unset($event['street']);
        unset($event['zip']);
        return $event;
    }

    public function unconfirmedEvent($datetime_now, $user_id, $request) {

        $event = Paginator::wrap(Event::join('invite', 'event.id', '=', 'invite.event_id')
                                ->where('event.date', '>', $datetime_now)
                                ->where('invite.user_id', '=', $user_id)
                                ->where('invite.type', '=', 'INVITE_RECEIVED')
                                ->addSelect('invite.id as invite_id'), $request);
        return $event;
    }

    /**
     * Ziskani ucastniku eventu
     *
     * @param Request $request
     * @param type $event_id
     */
    public function usersEvent(Request $request, $event_id) {
        $users = User::join('user_has_event', 'user_has_event.user_id', '=', 'users.id')
                ->where('user_has_event.event_id', $event_id)
                ->where('user_has_event.user_has_event', UserHasEvent::VISITOR);
        return Paginator::wrap($users, $request);
    }

    public function getPinEvent(Request $request, $pin_id) {

        $datetime_now = (new \DateTime())->format('Y-m-d H:i:s');

        $eventsfrompin = Event::where('pin_id', '=', $pin_id)
                        ->where('event.deleted_at', '=', NULL)
                        ->where('event.date', '>', $datetime_now)
                        ->where(function($query) {
                            $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
                            $user_school = User::where('id', '=', $user_id)->pluck('school');
                            return $query
                                    ->where('event.private', '=', 0)
                                    ->orWhere('event.user_id', '=', $user_id)
                                    ->orWhere('event.school_id', '=', $user_school);
                        })->distinct();

        return Paginator::wrap($eventsfrompin, $request);
        // return response()->json($return);
    }

    /*
     * Test Mapa Eventy
     */

    public function map(Request $request) {

        $pin_id = 2;
        $items = Event::where('pin_id', '=', $pin_id)->get();

        //$items = Item::distance(0.1,'45.05,7.6667')->get();
        return view('map')->with(['items' => $items]);
    }

}

/*
      public function get_lat_long($address, $city, $state){
      $fullAddress = $address." ".$city." ".$state;
      $urlAddress = str_replace(" ", "+",$fullAddress);
      $url = "http://maps.google.com/maps/api/geocode/json?address=";
      $url = $url.$urlAddress;
      $url = $url."&sensor=false&region=US";
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      $response = curl_exec($ch);
      curl_close($ch);
      $response_a = json_decode($response);
      $result['lat'] = $response_a->results[0]->geometry->location->lat;
      $result['lng'] = $response_a->results[0]->geometry->location->lng;
      return $result;
      }
    */
