<?php

namespace App\Http\Controllers;

use DB;
use App\Rate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Http\Response;
use LucaDegasperi\OAuth2Server\Authorizer;

class RateController extends Controller {


    public function __construct() {

    }
    
    public function rateEvent(Request $request,$id) {
        //user who create this rating
        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
        $add_input = array(
            'user_id' => $user_id
        );
         //rating number like 5.0
        $rating = number_format($request['rate'], 1);

        $Rate = Rate::updateOrCreate(

            ['user_id'  => (int)$user_id, 'event_id'  => (int)$id ],
            array(
                'user_id'  => (int)$user_id,
                'event_id'  => (int)$id,
                'rating'    => $rating,
                )
            );
        //nevim jestly je nutne
        $Rate->save();
        return response('', Response::HTTP_NO_CONTENT);
    }
}