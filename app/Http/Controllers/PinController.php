<?php

namespace App\Http\Controllers;

use App\Auth\User;
use Illuminate\Auth\AuthManager;
use LucaDegasperi\OAuth2Server\Authorizer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Utils\Paginator;
use App\Event;
use App\Pin;

class PinController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     *
     * @param Request $request
     * @return type
     */
    public function getPin(Request $request) {

        if (empty($_GET['longitude']) || empty($_GET['latitude']) || empty($_GET['distance'])) {
            $message = [
                'errorCode' => 81,
                'message' => 'Missing pin parameter'
            ];
            return response($message, Response::HTTP_BAD_REQUEST);
        }

        $longitude = $_GET['longitude'];
        $latitude = $_GET['latitude'];
        $distance = $_GET['distance'];

        //hledat v okruhu distance vsechny piny
        $lat_lon = $latitude . ',' . $longitude;

        $datetime_now = (new \DateTime())->format('Y-m-d H:i:s');

        $pin = Pin::distance((int) $distance, $lat_lon)
                        ->join('event', 'event.pin_id', '=', 'pin.id')
                        ->where('event.deleted_at', '=', NULL)
                        ->where('event.date', '>', $datetime_now)
                        ->where(function($query) {
                            $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
                            $user_school = User::where('id', '=', $user_id)->pluck('school');
                            return $query
                                    ->where('event.private', '=', 0)
                                    ->orWhere('event.user_id', '=', $user_id)
                                    ->orWhere('event.school_id', '=', $user_school);
                        })->distinct()->get();

        return response()->json($pin);
    }

    /*
     * @param Request $request
     * @return type
     */

    public function delete(Request $request) {

    }
}