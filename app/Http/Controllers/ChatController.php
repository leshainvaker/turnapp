<?php

namespace App\Http\Controllers;


use App\Utils\Paginator;

use App\Models\Chat;
use App\Models\ChatMessage;
use App\Models\Notification;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use DB;

class ChatController extends Controller {

    public function __construct() {

    }

    /**
     * Získání seznamu chatů
     */

    public function getChatList(Request $request){

        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();


	    /*
	     * join('invite', 'event.id', '=', 'invite.event_id')
                ->where('invite.user_id', '=', $user_id)
	     */

		/*
	    select chat.id, chat.user_id_from, chat.user_id_to,

		max(chat_messages.updated_at) as chatter

	    from `chat`

		inner join `chat_messages` on `chat`.`id` = `chat_messages`.`chat_id`

		where (`user_id_from` = 68 or `user_id_to` = 68)

		group by `chat`.`id`

		order by chatter desc*/


	    $chat_list = Chat::select('chat.id', 'chat.user_id_from', 'chat.user_id_to', DB::raw('max(chat_messages.id) as chatter'))
						    ->join('chat_messages', 'chat.id', '=', 'chat_messages.chat_id')
		                    ->where(function($q) use($user_id) {
			                $q->where('user_id_from', $user_id)
			                ->orWhere('user_id_to', $user_id);
		                    })->groupBy('chat.id')->orderBy(DB::raw('chatter'), 'desc')->get();

	    return response()->json($chat_list);


	    return response()->json($chat_list);

        //return Paginator::wrap($chat_list, $request);

    }

    /**
     * Vytvoření chatu
     */
    public function createChat(Request $request){

        $message        = $request->input('message');
        $subject        = $request->input('subject', '');
        $user_id_to     = $request->input('user_id');

        $user_id_from = app()->make('oauth2-server.authorizer')->getResourceOwnerId();


        // kouknu, jestli uz chat mezi uzivateli nahodou neexistuje. Pokud ano, pokracuji v nem.
        try {

            $chat = Chat::where(function($q) use ($user_id_from, $user_id_to) {
                $q->where('user_id_from', $user_id_from)->where('user_id_to', $user_id_to);
            })->orWhere(function($q) use ($user_id_from, $user_id_to) {
                $q->where('user_id_from', $user_id_to)->where('user_id_to', $user_id_from);
            })->firstOrFail();

            // vytvorim zpravu
            $this->createMessage($request, $chat->id);

        } Catch (ModelNotFoundException $e){

            // vytvorim chat a vlozim prvni zpravu
            $chat                   = new Chat();
            $chat->user_id_from     = $user_id_from;
            $chat->user_id_to       = $user_id_to;
            $chat->save();

            $this->createMessage($request, $chat->id);
            /*
            $chatMessage            = new ChatMessage();
            $chatMessage->message   = $message;
            $chatMessage->subject   = $subject;
            $chatMessage->chat_id   = $chat->id;
            $chatMessage->user_id   = $user_id_from;
            $chatMessage->save();
            */

            $chat->fresh();

        }

        return $chat;

    }


    /**
     * Získání zpráv v chatu
     */
    public function getMessage(Request $request, $chat_id){

        // oznacim zpravy jako prectene
        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
        ChatMessage::where('user_id', '!=', $user_id)->where('chat_id', $chat_id)->update(['message_state' => ChatMessage::STATE_READ]);
        return Paginator::wrap(ChatMessage::where('chat_id', (int) $chat_id)->orderBy('created_at', 'desc'), $request);
    }


    /**
     * Vytvoření zpráv v chatu
     */
    public function createMessage(Request $request, $chat_id){

        $user_id_from = app()->make('oauth2-server.authorizer')->getResourceOwnerId();

        $message                = new ChatMessage();
        $message->chat_id       = $chat_id;
        $message->message       = $request->input('message');
        $message->subject       = $request->input('subject');
        $message->user_id       = $user_id_from;

        $message->save();
        $message->fresh();

        // vytvorim notifikaci pro prijemce zpravy
        $chat = Chat::find($chat_id);
        $user_id_to = $user_id_from == $chat->user_id_from ? $chat->user_id_to : $chat->user_id_from;
        Notification::add($message->id, Notification::NEW_MESSAGE, $user_id_to, $user_id_from);

        return $message;
    }
}