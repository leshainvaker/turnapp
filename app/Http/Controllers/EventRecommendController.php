<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Auth\AuthManager;
use LucaDegasperi\OAuth2Server\Authorizer;
use App\Models\EventRecommend;
use App\Models\Notification;
use Illuminate\Http\Response;

class EventRecommendController {

    public function construct(){

    }

    /**
     * recommend to an event
     *
     * @param Request $request
     * @param type $event_id
     */
    public function send(Request $request, $event_id){

        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
        $send_to = json_decode($request->getContent());

        foreach( (array) $send_to as $user_id_to){

            $data = [
                'event_id'          => $event_id,
                'user_id_from'      => $user_id,
                'user_id_to'        => $user_id_to
            ];

            $recommend = EventRecommend::create($data);

            /**
             * Notifikace pro uzivatele, kterym prijde doporuceni
             */
            Notification::add($recommend->id, Notification::RECOMMENDATION_TO_EVENT, $user_id_to, $user_id);
        }


        return response('', Response::HTTP_NO_CONTENT);

    }


    public function get(Request $request, $event_id){



        


    }



}