<?php

namespace App\Http\Controllers;

use App\Auth\User;
use App\UserHasEvent;
use App\EventRequest;
use App\EventInvite;
use App\Models\Notification;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use LucaDegasperi\OAuth2Server\Authorizer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Utils\Paginator;

use DB;
use Image;



class UserController extends Controller {

    public function construct(){

    }

    public function user($user_id){

        try {

            $user = User::where('id', (int) $user_id)->firstOrFail();
            return response()->json($user);

        }
        Catch (ModelNotFoundException $e){

            $message = [
                'errorCode' => 50,
                'message' => 'User not found'
            ];

            return response($message, Response::HTTP_NOT_FOUND);
        }

    }


    public function userSearch(Request $request){

        if(!isset($_GET['search'])){

            $message = [
                'errorCode' => 51,
                'message' => 'Missing search parameter'
            ];

            return response($message, Response::HTTP_BAD_REQUEST);

        }

        return Paginator::wrap(User::where('username', 'LIKE', '%' . $_GET['search'] . '%'), $request);
    }


    public function me(){

        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
        $user = User::where('id', $user_id)->get();

        return response()->json($user);

    }


    public function delete(){

        try {
            $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
            $user = User::find($user_id)->firstOrFail();
            $user->delete();
            return (new Response('', Response::HTTP_NO_CONTENT));
        }
        Catch (ModelNotFoundException $e){
            $message = [
                'errorCode' => 50,
                'message' => 'User not found'
            ];

            return response($message, Response::HTTP_NOT_FOUND);
        }
        /*
        Catch(Exception $e){
            $message = [
                'errorCode' => 50,
                'message' => 'User not found'
            ];

            return response($message, Response::HTTP_BAD_REQUEST);
        }*/

    }


    public function update(Request $request){

        $rawInput = $request->getContent(false);
        $putData = json_decode($rawInput, true);


        $args = [
            //'username',
            //'email'
        ];



        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
        $user = User::find($user_id);


        if($putData){

            $request->merge($putData);



            if(!empty($request->input('email')) && $request->input('email') != $user->email){
                $args[] = 'email';
            }


            if(!empty($request->input('password'))){
                $args[] = 'password';
            }

            /*
            if(!empty($request->input('username'))){
                $args[] = 'username';
            }*/


            $validator = User::getValidator($args, $request);

            if ($validator->fails()) {

                $messages = $validator->errors();

                foreach ($messages->all() as $message) {

                    list($errorCode, $text) = explode('|', $message);

                    $return = [
                        'errorCode' => $errorCode,
                        'message' => $text
                    ];

                    return response($return, Response::HTTP_FORBIDDEN);
                }
            }


            //$user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
            //$user = User::find($user_id);

            $inputs = [
                //'username',
                //'email',
                'birthday',
                'description',
                'school',
                //'image'
            ];
 
            $data = [];

            foreach($inputs as $key){
                if(!empty($request->input($key))){
                    $data[$key] = $request->input($key);
                }
            }


            if($request->has('image')){

                try {
                    $img = Image::make($request->input('image'));
                    $data['image'] = (string) $img->encode("data-url");
                    
                    
                    $thumb = Image::make($request->input('image'));
                    $thumb->fit(300, 300);
                    $data['thumbnail'] = (string) $thumb->encode("data-url");
                }
                Catch (\Exception $e) {

                }

            }

            if(!empty($request->input('password'))){
                $data['password'] = Hash::make($request->input('password'));
            }

            if(!empty($request->input('email'))){
                $data['email'] = $request->input('email');
            }

            $user->update($data);
            $user->save();
            $user->fresh();
        }

        return response()->json($user);

    }


    /**
     * Odhlášení uživatele z eventu
     *
     * @param Request $request
     * @param type $event_id
     */
    public function cancelRegistrationToEvent(Request $request, $event_id){

        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();

        try {
            // najdu autora eventu
            $event = \App\Auth\User::join('user_has_event', 'user_has_event.user_id', '=', 'users.id')
                    ->join('event', 'event.id', '=', 'user_has_event.event_id')
                    ->select('user_has_event.user_id', 'event.capacity')
                    ->where('event.id', '=', $event_id)
                    ->where('user_has_event.user_has_event', '=', UserHasEvent::CREATOR)
                    ->firstOrFail();

            UserHasEvent::where('event_id', $event_id)
                    ->where('user_id', $user_id)
                    ->where('user_has_event', UserHasEvent::VISITOR)
                    ->delete();
 
	        // change request status
	        EventRequest::where('event_id', $event_id)
		                ->where('user_id', $user_id)
		                ->update(['type' => 'REQUEST_SELF_DECLINED']);

	        // change invite status
	        EventInvite::where('event_id', $event_id)
	                   ->where('user_id', $user_id)
	                   ->update(['type' => 'INVITE_DECLINED']);


            //add($object_id, $type, $user_id){
            Notification::add($event_id, Notification::CANCEL_REGISTRATION, $event->user_id, $user_id);
        }
        Catch (ModelNotFoundException $e) {

        }
    }
    
    public function image($user_id){
        
        //var_dump($user_id); exit;
        
        try {
            //$user = User::find($user_id)->firstOrFail();
            $user = User::where('id', $user_id)->first();
           
            //var_dump($user); exit;
            
            //var_dump($user->image); exit;
                        
            if(!empty($user->originalImage)){
                
                //var_dump($user->originalImage); exit;
                
                $img = Image::make($user->originalImage);
                echo $img->response('jpg', 70);
                
            }
            
        }
        Catch (ModelNotFoundException $e){
            
        }
        
        
        
    }
    
    public function thumbnail($user_id){
        
        try {
            //$user = User::find($user_id)->firstOrFail();
            $user = User::where('id', $user_id)->first();
            
                        
            if(!empty($user->thumbnail)){
                
                $img = Image::make($user->thumbnail);
                echo $img->response('jpg', 70);
                
            }
            
        }
        Catch (ModelNotFoundException $e){
            
        }        
        
    }

    public function showEmail($user_id) {
        try {

            $user = User::where('id', (int) $user_id)->firstOrFail();
            $user->hidemail = 0;
            $user->save();
            return response()->json($user);

        }
        Catch (ModelNotFoundException $e){

            $message = [
                'errorCode' => 50,
                'message' => 'User not found'
            ];

            return response($message, Response::HTTP_NOT_FOUND);
        }
    }

    public function hideEmail($user_id) {
        try {

            $user = User::where('id', (int) $user_id)->firstOrFail();
            $user->hidemail = 1;
            $user->save();
            return response()->json($user);

        }
        Catch (ModelNotFoundException $e){

            $message = [
                'errorCode' => 50,
                'message' => 'User not found'
            ];

            return response($message, Response::HTTP_NOT_FOUND);
        }
    }

    public function getFbUser(Request $request){

        $accessToken = $request->input('facebookAcessToken');
        $url = "https://graph.facebook.com/me?access_token=" . $accessToken;

        $userDetail = file_get_contents($url);
        //$facebookUserData = json_decode($userDetail, true);



        return $userDetail;

    }

}