<?php

namespace App\Http\Controllers;

use Illuminate\Auth\AuthManager;
use LucaDegasperi\OAuth2Server\Authorizer;

class ExampleController extends Controller
{
    
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
    
    public function test(){
        
        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();                
        return "test: _x[[-]]x_ : user_id: " . $user_id;        
        
    }

    //
}
