<?php

namespace App\Http\Controllers;

use Illuminate\Auth\AuthManager;
use LucaDegasperi\OAuth2Server\Authorizer;

use App\Models\School;
use Illuminate\Http\Request;
use App\Utils\Paginator;

use DB;

class SchoolController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }


    /**
     * získání seznamu škol
     */
    public function all(Request $request){
        return Paginator::wrap(School::query(), $request);
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function byId($id){
        return School::where('id', (int) $id)->get();
    }

    
    public function search(Request $request){
        
        $searchQuery = $request->input('query', '');
        
        return Paginator::wrap(School::where('name', 'LIKE', '%' . $searchQuery . '%'), $request);
                
    }
    
    //
}

