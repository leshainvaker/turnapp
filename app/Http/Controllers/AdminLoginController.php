<?php

namespace App\Http\Controllers;

use Illuminate\Auth\AuthManager;
use LucaDegasperi\OAuth2Server\Authorizer;
use Illuminate\Http\Request;

class AdminLoginController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        
    }

    public function login(Request $request) {

        //$user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();                
        //return "test: _x[[-]]x_ : user_id: " . $user_id;        

        return view('login');
    }

    public function processLogin(Request $request) {


        try {
            $input = array(
                'grant_type' => 'password',
            );

            $request->merge($input);

            $authorizer = app()->make('oauth2-server.authorizer');
            $accessToken = $authorizer->issueAccessToken();
            
            $url = route('baseAdmin') . '?access_token=' . $accessToken['access_token'];
            return redirect($url);
            
            //$r = response()->json($accessToken);
            //return $r;
        } Catch (\Exception $e) {
            
            $message = $e->getMessage();
            return view('login', ['error' => $message]);
            
        }
        //
    }
    
    public function logout(){
        
        app()->make('oauth2-server.authorizer')->getChecker()->getAccessToken()->expire();
        return redirect()->route('adminLogin');
        
    }
}
    