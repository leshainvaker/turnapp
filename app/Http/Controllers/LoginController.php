<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Auth\User;
use App\Models\UserData;
use App\Models\UserFacebook;
use App\Models\UserTwitter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
//use Illuminate\Validation\Validator;
use Validator;
use Illuminate\Http\Response;
use LucaDegasperi\OAuth2Server\Authorizer;
use League\OAuth2\Server\Entity\SessionEntity;

class LoginController extends Controller {

    /**
     *
     * @param Request $request
     * @return type
     */
    public function login(Request $request) {

        try {
            return $this->processLogin($request);
        } Catch (\Exception $e) {

            $message = [
                'errorCode' => $e->getCode(),
                'message' => $e->getMessage()
            ];

            return response($message, Response::HTTP_BAD_REQUEST);
        }
    }

    protected function processLogin(Request $request) {

        //
        //
        //var_dump($request); exit;
        //var_dump($request); exit;
        //$json = $request->input('json_decode');

        $body = $request->getContent();



        //if (!empty($body)) {

        $input = array(json_decode($body, true));

        //var_dump($input); exit;

        if ($input) {

            foreach ($input as $val) {
                if (!empty($val)) {
                    $request->merge($val);
                }
            }
        }
        //}
        //var_dump($input); exit;
        //$requestContent = \json_decode($request->input);
        //var_dump($requestContent);


        if ($request->has('email') && $request->has('password')) {
            $token = $this->credentialsLogin($request);
            $userData = UserData::getInstance();
            $user = $userData->getUser();
        } elseif ($request->has('twitterToken') && $request->has('twitterSecret')) {
            $user = UserTwitter::fromApi($request);

            $input = array(
                'grant_type' => 'twitter_token',
            );

            $request->merge($input);

            /**
             * user se pak pouzije v TwitterTokenGrant
             */
            UserData::getInstance()->setUser($user);
            $token = response()->json(app()->make('oauth2-server.authorizer')->issueAccessToken());
        } elseif ($request->has('facebookAcessToken')) {

            /**
             * Vrati uzivatele vytvoreneho pres facebook api, jestli neexistuje, vytvori ho
             */
            $user = UserFacebook::fromApi($request);

            $input = array(
                'grant_type' => 'facebook_token',
                    //'username' => $newUser->email
            );

            $request->merge($input);

            /**
             * user se pak pouzije v FacebookTokenGrant
             */
            UserData::getInstance()->setUser($user);
            $token = response()->json(app()->make('oauth2-server.authorizer')->issueAccessToken());
        } else {
            throw new \Exception('Missing parameter', 10);
        }

	    //update device token
	    if(!empty($request->input('device_token'))) {

		    $data['device_token'] = $request->input('device_token');
		    $user->update($data);
		    $user->save();

	    }

        //$email = empty($user->email) ? '' : $user->email;

        $content = [
            'token' => json_decode($token->getContent()),
            'user' => $user->fresh()
        ];

        $response = response($content, Response::HTTP_OK);
        return $response;
    }

    /**
     *
     * @param Request $request
     * @return type
     */
    public function credentialsLogin(Request $request) {


        $input = array(
            'grant_type' => 'password',
        );

        $request->merge($input);

        $authorizer = app()->make('oauth2-server.authorizer');
        $accessToken = $authorizer->issueAccessToken();
        $r = response()->json($accessToken);
        return $r;
    }

}
