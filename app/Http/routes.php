<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

$app->get('image/user/{user_id:\d+}', ['uses' => 'UserController@image', 'as' => 'userImage']);
$app->get('image/user/{user_id:\d+}/thumbnail', ['uses' => 'UserController@thumbnail', 'as' => 'userThumbnail']);


$app->get('image/event/{image_id:\d+}', ['uses' => 'EventPhotoController@image', 'as' => 'eventImage']);
$app->get('image/event/{image_id:\d+}/thumbnail', ['uses' => 'EventPhotoController@thumbnail', 'as' => 'eventThumbnail']);


$app->get('callback', function () {
    return '';
});


$app->post('callback', function () {
    return '';
});


$app->post('access_token', function() {
    $token = response()->json(app()->make('oauth2-server.authorizer')->issueAccessToken());
});
/**
 * Sem všechny routy, které jsou zabezpečeny pomocí oauth
 */
$app->group(['middleware' => ['oauth-exception', 'oauth'], 'namespace' => 'App\Http\Controllers'], function () use ($app) {


    $app->post('test', 'ExampleController@test');
    /**
     */
    $app->delete('token', 'LogoutController@logout');
    /**
     */
    $app->get('user/{user_id:\d+}', 'UserController@user');
    $app->get('emailhide/{user_id:\d+}', 'UserController@hideEmail');
    $app->get('emailshow/{user_id:\d+}', 'UserController@showEmail');
    $app->post('getfbuser', 'UserController@getFbUser');
    /**
     */
    $app->get('user', 'UserController@userSearch');
    $app->get('user/{user_id:\d+}', 'UserController@user');
    $app->get('user/me', 'UserController@me');

    $app->delete('user/me', 'UserController@delete');
    $app->put('user/me', 'UserController@update');

    /**
     * Informace o aktualnim tokenu
     */
    $app->get('token', 'TokenController@token');
    /**
     * Recommend event
     */
    $app->post('event/{event_id:\d+}/recommend', 'EventRecommendController@send');


    $app->get('event/{event_id:\d+}/recommend', 'EventRecommendConroller@get');
    /*
     * EVENT ENTITA
     */
    //Vytvoření eventu
    $app->post('event', 'EventController@createEvent');

    //Získání eventu
    $app->get('event/{event_id:\d+}', 'EventController@getEvent');

    //Zruseni eventu
    $app->delete('event/{event_id:\d+}', 'EventController@deleteEvent');

    //Editace eventu
    $app->put('event/{event_id:\d+}', 'EventController@updateEvent');

    //Ziskani eventu ktere navstivim
    $app->get('event/attending', 'EventController@getEventAttending');

    //Ziskani eventu ktere poradam
    $app->get('event/organizing', 'EventController@getEventOrganizing');

    //Ziskani vsech eventu od konkretniho uzivatele
    $app->get('event/user/{user_id:\d+}', 'EventController@getEventFromUser');

    //Ziskani ucastniku akce
    $app->get('event/{event_id:\d+}/user', 'EventController@usersEvent');

    //Ziskani obrazku k eventu
    $app->get('event/{event_id:\d+}/image', 'EventPhotoController@get');

    //Nahrani obrazku k eventu
    $app->post('event/{event_id:\d+}/image', 'EventPhotoController@add');

    //Odstraneni obrazku k eventu
    $app->delete('event/{event_id:\d+}/image/{image_id:\d+}', 'EventPhotoController@delete');

    //Odhlášení se z eventu
    $app->delete('event/{event_id:\d+}/registration', 'UserController@cancelRegistrationToEvent');


    //Ziskani pinu na mape
    $app->get('pin', 'PinController@getPin');


    $app->get('map_pin/{map_pin_id:\d+}/event', 'EventController@getPinEvent');

    /*
     * Event/Ohodnocení eventu/Ohodnocení eventu
     */
    $app->put('event/{event_id:\d+}/rate', 'RateController@rateEvent');


    /**
     * Event/Poznámky k eventu/Získání poznámek k eventu
     */
    $app->get('event/{event_id:\d+}/note', 'NoteController@getNote');
    /**
     * Pridani poznamky k eventu
     */
    $app->post('event/{event_id:\d+}/note', 'NoteController@addNote');

    /**
     * Editace poznamky k eventu
     */
    $app->put('event/note/{note_id:\d+}', 'NoteController@editNote');
    /**
     * Smazani poznamky k eventu
     */
    $app->delete('event/note/{note_id:\d+}', 'NoteController@deleteNote');



    /*
     * CHAT
     */
    //ziskani seznamu chatu
    $app->get('chat', 'ChatController@getChatList');
    //vytvoreni chatu
    $app->post('chat', 'ChatController@createChat');

    //ziskani zprav chatu
    $app->get('/chat/{chat_id}/message', 'ChatController@getMessage');
    //vytvoreni zprav v chatu
    $app->post('/chat/{chat_id}/message', 'ChatController@createMessage');


    /*
     * REQUEST
     */
    $app->get('/event/{event_id:\d+}/request', 'EventRequestController@getRequest');
    $app->post('/event/{event_id:\d+}/request', 'EventRequestController@createRequest');
    $app->delete('/event/{event_id:\d+}/request/{request_id:\d+}', 'EventRequestController@deleteRequest');
    $app->put('/event/{event_id:\d+}/request/{request_id:\d+}', 'EventRequestController@updateRequest');

    /*
     * INVITE
     */
    $app->get('/event/{event_id:\d+}/invite', 'EventInviteController@getInvite');
    $app->post('/event/{event_id:\d+}/invite', 'EventInviteController@createInvite');
    $app->delete('/event/{event_id:\d+}/invite/{invite_id:\d+}', 'EventInviteController@deleteInvite');
    $app->put('/event/{event_id:\d+}/invite/{invite_id:\d+}', 'EventInviteController@updateInvite');

    /*
     * SCHOOLS
     */
    $app->get('school', 'SchoolController@all');
    $app->get('school/{id:\d+}', 'SchoolController@byId');
    $app->get('school/search', 'SchoolController@search');

    /*
     * NOTIFICATION
     *
     */
    $app->get('notification', 'NotificationController@all');
    $app->get('/notification/{event_id:\d+}', 'NotificationController@three_hours_before');
    $app->post('/notification/{notification_id:\d+}/read', 'NotificationController@read');

	/*
     * PUSH NOTIFICATION
     *
     */
	$app->get('pushnotification', 'PushNotificationController@sendNotificationToDevice');

	$app->get('pushnotificationfcm', 'PushNotificationController@sendNotificatonFCM');


});


/**
 * Routy zabezpečené pomocí API-KEY v headeru
 */
$app->group(['middleware' => 'api-key', 'namespace' => 'App\Http\Controllers'], function ($request) use ($app) {

    /**
     *
     */
    $app->post('user', 'RegisterController@register');

    /**
     *
     */
    $app->post('token', 'LoginController@login');

    /**
     *
     */
    $app->post('password_recovery', 'PasswordController@recovery');
});


    //nezabezpecene testovaci routy - nutno smazat nebo vyresit autentifikaci


    $app->get('map', 'EventController@map');


    $app->get('map_pin/{map_pin_id:\d+}', 'EventController@getMapPin');


    $app->get('map_search', 'PinController@mapsearch');


/*
 *
 * {
  "token": {
    "access_token": "3TVfxqyBoRufdBT5U7riLs0SnfSAeCf4YmG9CG73",
    "token_type": "Bearer",
    "expires_in": 360000,
    "refresh_token": "2f7tjcfzj7gZJindrAcDhFHj6780VyMGmiaSiltx"
  },
  "user": {
    "email": "jimmy@hendrix.com",
    "username": "Jimmy Hendrix"
  }
}
 *
 *
 *
 */



/**
*
*/
$app->get('/admin/login', ['uses' => 'AdminLoginController@login', 'as' => 'adminLogin']);
$app->post('/admin/login', ['uses' => 'AdminLoginController@processLogin', 'as' => 'adminProcessLogin']);


/**
 * Routy do administrace
 *
 */
//'middleware' => ['auth.basic', 'auth.admin']
$app->group(['middleware' => ['adminAuth'], 'namespace' => 'App\Http\Controllers'], function () use ($app) {

    $app->get('/admin/logout', ['uses' => 'AdminLoginController@logout', 'as' => 'adminLogout']);

    /**
     * Administrace eventy
     */
    $app->get('/admin', ['uses' => 'AdminEventController@eventList', 'as' => 'baseAdmin']);
    $app->get('/admin/events', ['uses' => 'AdminEventController@eventList', 'as' => 'eventList']);
    $app->get('/admin/events/filter', ['uses' => 'AdminEventController@filter', 'as' => 'eventFilter']);
    $app->get('/admin/event/{event_id}', ['uses' => 'AdminEventController@edit', 'as' => 'eventEdit']);
    $app->get('/admin/event/{event_id:\d+}/photos', ['uses' => 'AdminEventController@photos', 'as' => 'eventPhotos']);

    $app->post('/admin/event/{event_id:\d+}', ['uses' => 'AdminEventController@save', 'as' => 'eventSave']);
    $app->post('/admin/event/{event_id:\d+}/delete', ['uses' => 'AdminEventController@delete', 'as' => 'eventDelete']);
    $app->post('/admin/event/{event_id:\d+}/restore', ['uses' => 'AdminEventController@restore', 'as' => 'eventRestore']);
    $app->post('/admin/event/{event_id:\d+}/delete-image/{image_id}', ['uses' => 'AdminEventController@deleteImage', 'as' => 'eventDeleteImage']);
    $app->post('/admin/event/{event_id:\d+}/image', ['uses' => 'AdminEventController@saveImage', 'as' => 'eventPhotosSave']);


    /**
     * Administrace uzivatele
     */
    $app->get('/admin/users', ['uses' => 'AdminUserController@userList', 'as' => 'userList']);
    $app->get('/admin/users/filter', ['uses' => 'AdminUserController@filter', 'as' => 'userFilter']);
    $app->get('/admin/user/{user_id:\d+}', ['uses' => 'AdminUserController@edit', 'as' => 'userEdit']);
    $app->post('/admin/user/{user_id:\d+}', ['uses' => 'AdminUserController@save', 'as' => 'userSave']);
    $app->post('/admin/user/{user_id:\d+}/delete', ['uses' => 'AdminUserController@delete', 'as' => 'userDelete']);
    $app->post('/admin/user/{user_id:\d+}/delete-image', ['uses' => 'AdminUserController@deleteImage', 'as' => 'userDeleteImage']);

});