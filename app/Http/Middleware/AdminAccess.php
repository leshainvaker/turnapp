<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Middleware;

use Closure;
use App\Models\UserRole;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();        
        
        try {
            $admin = UserRole::where('user_id', $user_id)->where('role', 'admin')->firstOrFail();        
        }
        Catch (ModelNotFoundException $e) {
            return redirect()->route('adminLogin');
        }        
        
        return $next($request);
    }
}