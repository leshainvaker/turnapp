<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class ExpandToken {

    public function handle($request, Closure $next) {
        $response = $next($request);

        // Perform action
        
        $accessToken = app()->make('oauth2-server.authorizer')->getChecker()->getAccessToken();

        if ($accessToken) {

            $tokenId = $accessToken->getId();

            $config = app()['config']->get('oauth2');

            $new_expire_time = time() + (int) $config['access_token_ttl'];

            DB::table('oauth_access_tokens')
                    ->where('oauth_access_tokens.id', $tokenId)
                    ->update(['expire_time' => $new_expire_time]);
        }
        
        return $response;
    }

}
