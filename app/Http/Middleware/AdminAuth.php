<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Optimus\OAuth2Server\Middleware\OAuthMiddleware;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Routing\Middleware;
use League\OAuth2\Server\Exception\OAuthException;
use League\OAuth2\Server\Exception\AccessDeniedException;

class AdminAuth extends OAuthMiddleware implements Middleware {

    public function handle($request, Closure $next) {

        try {
            
            //$user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
            
            $return = parent::handle($request, $next);
            
            //$user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
        } 
        catch (AccessDeniedException $e) {
          
            /*
            return new JsonResponse([
                    'error'             => 3,
                    'error_description' => 'Access denied'
                ],
                    
                $e->httpStatusCode,
                $e->getHttpHeaders()
            );            
            */
            return redirect()->route('adminLogin');
        }
        catch (OAuthException $e) {
            
            return redirect()->route('adminLogin');

            /*
            return new JsonResponse([
                    'error'             => $e->errorType,
                    'error_description' => $e->getMessage()
                ],
                $e->httpStatusCode,
                $e->getHttpHeaders()
            );
            */
        }
        
        return $return;
        
        //$x= 1;
        /*
        if(!$request->has('access_token')){
            return redirect()->route('adminLogin');
        }*/
        
        
        
        
        
        /*
        $client_api_key = $request->header('API-KEY');
        $api_key = config('app.api_key');

        try {
            if (empty($client_api_key)) {
                throw new \Exception('Missing Api Key', 1);
            }

            if ($client_api_key != $api_key) {
                throw new \Exception('Invalid Api Key', 2);
            }
        } Catch (\Exception $e) {
            $return = [
                'errorCode' => $e->getCode(),
                'message' => $e->getMessage()
            ];

            return response($return, Response::HTTP_UNAUTHORIZED);
        }
        return $next($request);
        */
    }
}