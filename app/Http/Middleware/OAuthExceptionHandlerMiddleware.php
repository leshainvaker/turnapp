<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Routing\Middleware;
use League\OAuth2\Server\Exception\OAuthException;
use League\OAuth2\Server\Exception\AccessDeniedException;

/*
* OAuthExceptionHandlerMiddleware
*/
class OAuthExceptionHandlerMiddleware implements Middleware
{
    public function handle($request, Closure $next)
    {
        try {

            return $next($request);

        } 
        catch (AccessDeniedException $e) {
          
            
            return new JsonResponse([
                    'error'             => 3,
                    'error_description' => 'Access denied'
                ],
                    
                $e->httpStatusCode,
                $e->getHttpHeaders()
            );            
            
        }
        catch (OAuthException $e) {

            return new JsonResponse([
                    'error'             => $e->errorType,
                    'error_description' => $e->getMessage()
                ],
                $e->httpStatusCode,
                $e->getHttpHeaders()
            );
        }
    }
}
