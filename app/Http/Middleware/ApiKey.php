<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;


class ApiKey {

    public function handle($request, Closure $next) {

        $client_api_key = $request->header('API-KEY');
        $api_key = config('app.api_key');

        try {
            if (empty($client_api_key)) {
                throw new \Exception('Missing Api Key', 1);
            }

            if ($client_api_key != $api_key) {
                throw new \Exception('Invalid Api Key', 2);
            }
        } Catch (\Exception $e) {
            $return = [
                'errorCode' => $e->getCode(),
                'message' => $e->getMessage()
            ];

            return response($return, Response::HTTP_UNAUTHORIZED);
        }
        return $next($request);
    }
}
