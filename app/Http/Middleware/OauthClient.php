<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace App\Http\Middleware;

use Closure;



class OauthClient {

    public function handle($request, Closure $next) {

        $config = app()->make('config');
        
        $input = [
            'client_id'     => $config->get('secrets.client_id'),
            'client_secret' => $config->get('secrets.client_secret')
        ];
        
        $request->merge($input);

        return $next($request);
    }
}