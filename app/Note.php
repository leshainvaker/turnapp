<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model {

    //use SoftDeletes;

    //public $timestamps = false;
    protected $table = 'note';
    protected $fillable = ['id','note_title','note', 'event_id'];
}

?>