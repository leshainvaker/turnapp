<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AppException extends Exception {
    
    protected $status;
    
    
    public function construct($message = '', $code = '', $status = ''){        
        parent::construct($message, $code);        
        $this->status = $status;
    }
    
    
    public function getStatus(){
        return $this->status();
    }
 
    
}