<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function tokenRoute($name, $params = array()) {

    $token = '';

    if (isset($_GET['access_token'])) {        
        $token = 'access_token=' . htmlspecialchars($_GET['access_token']);
    }

    $route = route($name, $params);
    
    $glue = false === strpos($route, '?') ? '?' : '&';
    
    return $route . $glue . $token;
    
}

function getTokenToForm (){
    
    $token = '';
    
    if(isset($_GET['access_token'])){
        $token = htmlspecialchars($_GET['access_token']);
    }
    
    return '<input type="hidden" name="access_token" value="' . $token . '">';
}
