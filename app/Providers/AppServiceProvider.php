<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\UserHasEvent;
use App\Event;
use App\Models\Notification;

use App\Models\PushNotificationModel;

//push notifikace

use LucaDegasperi\OAuth2Server\Authorizer;
use App\Auth\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;


use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

use FCM;



use DB;


//use Illuminate\Contracts\Cookie\QueueingFactory;
//use Illuminate\Cookie\CookieJar;

class AppServiceProvider extends ServiceProvider {

	public function boot() {

		UserHasEvent::saved( function ( $userHasEvent ) {

			/**
			 * jedna se o vytvoreni nove eventy, vytvoreni vztahu do tabule UserHasEvent pro autora, rovnou vyskocim
			 */
			if ( $userHasEvent->user_has_event == UserHasEvent::CREATOR ) {
				return;
			}

			// kouknu jestli eventa neni nahodou plna, pokud ano vytvorim notifikaci
			$participantCount = UserHasEvent::Where( 'user_id', '=', $userHasEvent->user_id )->where( 'event_id', '=', $userHasEvent->event_id )->where( 'user_has_event', '=', UserHasEvent::VISITOR )->count();

			try {
				//$event = Event::where('id', $userHasEvent->event_id)->firstOrFail();


				$event = DB::table( 'users' )
				           ->join( 'user_has_event', 'user_has_event.user_id', '=', 'users.id' )
				           ->join( 'event', 'event.id', '=', 'user_has_event.event_id' )
				           ->select( 'user_has_event.user_id', 'event.capacity' )
				           ->where( 'event.id', '=', $userHasEvent->event_id )
				           ->where( 'user_has_event.user_has_event', '=', UserHasEvent::CREATOR )
				           ->first();


				if ( $participantCount >= $event->capacity ) {
					// event is full
					Notification::add( $userHasEvent->event_id, Notification::EVENT_FILLED, $event->user_id, null );
				}

			} Catch ( ModelNotFoundException $e ) {

				/*
				$message = [
					'errorCode' => 70,
					'message' => 'Requested event not found'
				];
				*/
				//return response($message, Response::HTTP_NOT_FOUND);
			}

		} );

		/**
		 *
		 */
		Event::deleted( function ( $event ) {

			/**
			 * autor eventu..
			 */
			$user = DB::table( 'users' )
			          ->join( 'user_has_event', 'user_has_event.user_id', '=', 'users.id' )
			          ->join( 'event', 'event.id', '=', 'user_has_event.event_id' )
			          ->select( 'user_has_event.user_id' )
			          ->where( 'event.id', '=', $event->id )
			          ->where( 'user_has_event.user_has_event', '=', UserHasEvent::CREATOR )
			          ->first();
			/**
			 * notifikace pro vsechny navstevniky o zruseni eventu..
			 */
			Notification::eventNotification( $event->id, Notification::CANCELED_EVENT, $user->user_id );

		} );

		/*
		 * Posli notifikaci
		 *
		 */

		Notification::saved( function ( $notification ) {
			 /*
			 * TODO: switch
			 *$notification->type;
             */
			return print_r( $notification );

			$message = PushNotificationModel::getConstant( $notification->type['message'] );

			var_dump( $message );

			$user        = User::find( $notification->user_id_to );
			$token = $user->device_token;

			var_dump($token);

			$optionBuiler = new OptionsBuilder();
			$optionBuiler->setTimeToLive(60*20);

			$notificationBuilder = new PayloadNotificationBuilder('my title');
			$notificationBuilder->setBody('Hello world notification')
			                    ->setSound('default');
			/*
			$dataBuilder = new PayloadDataBuilder();
			$dataBuilder->addData(['a_data' => 'my_data']);
			*/
			$option = $optionBuiler->build();
			$notification = $notificationBuilder->build();

			/*$data = $dataBuilder->build();*/

			$downstreamResponse = FCM::sendTo($token, $option, $notification); //, $data);

			$downstreamResponse->numberSuccess();
			$downstreamResponse->numberFailure();
			$downstreamResponse->numberModification();


			var_dump($downstreamResponse);
			//return Array - you must remove all this tokens in your database
			$tokendelete = $downstreamResponse->tokensToDelete();
			var_dump($tokendelete);
			//return Array (key : oldToken, value : new token - you must change the token in your database )
			$tokentomodify = $downstreamResponse->tokensToModify();
			var_dump($tokentomodify);
			//return Array - you should try to resend the message to the tokens in the array
			$tokentoreply = $downstreamResponse->tokensToRetry();
			var_dump($tokentoreply);


		});
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		//
		$this->app->bind( Illuminate\Contracts\Cookie\QueueingFactory::class, Illuminate\Cookie\CookieJar::class );
	}
}
