<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\UserHasEvent;
use App\Event;
use App\Models\Notification;

use DB;

//use Illuminate\Contracts\Cookie\QueueingFactory;
//use Illuminate\Cookie\CookieJar;

class AppServiceProvider extends ServiceProvider
{

    public function boot(){

        UserHasEvent::saved(function ($userHasEvent){

            /**
             * jedna se o vytvoreni nove eventy, vytvoreni vztahu do tabule UserHasEvent pro autora, rovnou vyskocim
             */
            if($userHasEvent->user_has_event == UserHasEvent::CREATOR){
                return;
            }

            // kouknu jestli eventa neni nahodou plna, pokud ano vytvorim notifikaci
            $participantCount = UserHasEvent::Where('user_id', '=', $userHasEvent->user_id)->where('event_id', '=' , $userHasEvent->event_id)->where('user_has_event', '=', UserHasEvent::VISITOR)->count();

            try {
                //$event = Event::where('id', $userHasEvent->event_id)->firstOrFail();


                $event =    DB::table('users')
                            ->join('user_has_event', 'user_has_event.user_id', '=', 'users.id')
                            ->join('event', 'event.id', '=', 'user_has_event.event_id')
                            ->select('user_has_event.user_id', 'event.capacity')
                            ->where('event.id', '=', $userHasEvent->event_id)
                            ->where('user_has_event.user_has_event', '=', UserHasEvent::CREATOR)
                            ->first();


                if($participantCount >= $event->capacity){
                    // event is full
                    Notification::add($userHasEvent->event_id, Notification::EVENT_FILLED, $event->user_id, null);

                }

            }
            Catch (ModelNotFoundException $e)
            {

                /*
                $message = [
                    'errorCode' => 70,
                    'message' => 'Requested event not found'
                ];
*/
                //return response($message, Response::HTTP_NOT_FOUND);
            }

        });



        /**
         *
         */
        Event::deleted(function ($event){

            /**
            * autor eventu..
            */
            $user = DB::table('users')
                    ->join('user_has_event', 'user_has_event.user_id', '=', 'users.id')
                    ->join('event', 'event.id', '=', 'user_has_event.event_id')
                    ->select('user_has_event.user_id')
                    ->where('event.id', '=', $event->id)
                    ->where('user_has_event.user_has_event', '=', UserHasEvent::CREATOR)
                    ->first();

            /**
            * notifikace pro vsechny navstevniky o zruseni eventu..
            */
            Notification::eventNotification($event->id, Notification::CANCELED_EVENT, $user->user_id);

        });

    }


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(Illuminate\Contracts\Cookie\QueueingFactory::class, Illuminate\Cookie\CookieJar::class);
    }
}
