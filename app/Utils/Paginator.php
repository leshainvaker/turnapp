<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Utils;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;


class Paginator {

    public static function wrap(QueryBuilder $queryBuilder, Request $request){

        $offset         = $request->input('offset', 0);
        $limit          = $request->input('limit', 30);

        $totalCount     = $queryBuilder->count();

        $links          = [];

        /**
         * previous
         */
        if($offset > 0){
            $links['prev'] = [
                'limit'     => $limit,
                'offset'    => $offset - $limit > 0 ? $offset - $limit : 0
            ];
        }

        /**
         * next
         */
        if($offset + $limit < $queryBuilder->count()){
            $links['next'] = [
                'limit'     => $limit,
                'offset'    => $offset + $limit
            ];
        }

        /**
         * current
         */
        $links['self'] = [
            'limit'         => $limit,
            'offset'        => $offset
        ];


        $data = [
            'offset'        => (int) $offset,
            'limit'         => (int) $limit,
            'pages'         => ceil($totalCount / $limit),
            'totalCount'    => $totalCount,
            'items'         => $queryBuilder->skip($offset)->take($limit)->get()
        ];

        $ret = [
            'link'          => $links,
            'data'          => $data
        ];

 
        //404 kdyz je vysledek prazdny - nevim kam to jinam dat

        return $ret;

    }
}
