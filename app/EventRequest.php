<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Auth\User;


class EventRequest extends Model {

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    //public $timestamps = false;
    protected $table = 'request';
    protected $fillable = ['id', 'type', 'event_id', 'user_id', 'created_at', 'updated_at'];

    //chteji do requestu take uzivatele

    protected $appends = ['user'];
 
    public function getUserAttribute(){
        if(isset($this->attributes['user_id'])){
            $user = User::where('id', $this->attributes['user_id'])->first();
            return $user;
        }
    }
    public function event() {
        return $this->belongsTo('Event');
    }
}
?>