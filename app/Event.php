<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\EventPhoto;
use App\Models\School;
use App\Rate;
use App\EventRequest;
use App\Auth\User;
use DB;
use App\Models\Notification;
use Illuminate\Http\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Event extends Model {

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $table = 'event';
    protected $fillable = ['id', 'name', 'date', 'capacity', 'indoor', 'apartament', 'street', 'city', 'number', 'zip', 'private', 'school_id', 'description', 'user_id', 'gps', 'pin_id'];

    protected $casts = [
        'indoor' => 'boolean',
        'private' => 'boolean',
        'school_id' => 'integer',
        'user_id' => 'integer',
    ];

    protected $hidden = ['original', 'thumbnail', 'created_at', 'updated_at'];
    protected $appends = ['user','images','rating','participiants','registred','unrequest','myrequest','canceled', 'school'];
    protected $geofields = array('gps');



    public function getUserAttribute(){
        if(isset($this->attributes['user_id'])){
            $user = User::where('id', $this->attributes['user_id'])->firstOrFail();
            return $user;
        }
    }

    public function getImagesAttribute() {

        $images = EventPhoto::where('event_id', '=', $this->attributes['id'])->get();
        return $images;


    }

    public function getSchoolAttribute() {

        $school = School::where('id', '=', $this->attributes['school_id'])->get();
        return $school;

    }

    public function getRatingAttribute() {
        $rating = Rate::where('event_id', '=', $this->attributes['id'])->avg('rating');
        return $rating;
    }


	/**
	 * @return mixed
	 */
	public function getMyrequestAttribute() {

		$user_id = app()->make( 'oauth2-server.authorizer' )->getResourceOwnerId();
		$request = EventRequest::where( 'user_id', '=', $user_id )
		                       ->where( 'event_id', '=', $this->attributes['id'] )
		                       ->take( 1 )
		                       ->orderBy( 'id', 'desc' )
		                       ->get();

		return $request;

	}

    public function getParticipiantsAttribute() {

        $users = User::join('user_has_event', 'user_has_event.user_id', '=', 'users.id')
                ->where('user_has_event.event_id', '=', $this->attributes['id'])
                ->where('user_has_event.user_has_event', '=', UserHasEvent::VISITOR)
                ->count();
        return $users;

    }

    public function getRegistredAttribute() {

        $count_request = Event::join('request', 'request.event_id', '=', 'event.id')
                        ->where('event.id', '=', $this->attributes['id'])
                        ->count();
        $count_invite = Event::join('invite', 'invite.event_id', '=', 'event.id')
                        ->where('event.id', '=', $this->attributes['id'])
                        ->count();
        return $count_request+$count_invite;

    }

    public function getUnrequestAttribute () {
        $event = Event::join('request', 'request.event_id', '=', 'event.id')
                        ->where('event.id', '=', $this->attributes['id'])
                        ->where('request.type', '=', 'REQUEST_RECEIVED' )
                        ->count();
        return $event;
    }

    public function getCanceledAttribute() {
        return $this->trashed();
    }

    //overide method for query and gps

    public function newQuery($excludeDeleted = false) {
        $raw = '';
        foreach ($this->geofields as $column) {
            $raw .= ' astext(' . $column . ') as ' . $column . ' ';
        }

        return parent::newQuery($excludeDeleted)->addSelect('event.*', DB::raw($raw));
    }

    public function setGpsAttribute($value) {

        $location = explode(" ", $value);
        $loc = $location[1].' '.$location[0];
        $this->attributes['gps'] = DB::raw("POINTFROMTEXT('POINT($loc)')");

    }

    public function getGpsAttribute($value)
    {

        $offset = strrpos($value, 'POINT');
        $loc = substr($value, $offset);
        $loc =  substr($loc, 6);
        $loc = preg_replace('/[ ,]+/', ',', $loc, 1);
        $loc = strstr($loc, ')', true);
        $location = explode(",", $loc);

        $loc = $location[1].','.$location[0];
        return $loc;

    }


    public function scopeDistance($query, $dist, $location) {

       $location = explode(",", $location);
       $loc = $location[1].','.$location[0];

       //return $query->whereRaw('st_distance(gps,POINT(' . $loc . ')) < ' . $dist);

       return $query->whereRaw('st_distance_sphere(event.gps,POINT(' . $loc . ')) < ' . $dist);

    }

    public function event() {

        return $this->belongsToMany('Users', 'user_has_event', 'event_id', 'user_id');

    }

    public function eventrequest() {

        return $this->hasMany('EventRequest');

    }


    public function eventinvite() {

        return $this->hasMany('EventInvite');

    }

    public function school() {
        return $this->belongsTo('App\Models\School');
    }

    public static function randDistance($lat, $lon, $dist) {

        //Earth’s radius, sphere
        $Radius = 6378137;
        $Pi = pi();
        //offsets in meters
        $dn = 500;
        $de = 500;
        //Coordinate offsets in radians
        $dLat = $dn / $Radius;
        $dLon = $de / ($Radius * cos($Pi * $lat / 180));
        //OffsetPosition, decimal degrees
        $latplus = $lat + $dLat * 180 / $Pi;
        $lonplus = $lon + $dLon * 180 / $Pi;

        $response[0] = $latplus;
        $response[1] = $lonplus;

        return $response;

    }

    public static function getCoordinates($address) {

        $address = rawurlencode($address);
        $url = "https://maps.google.com/maps/api/geocode/json?sensor=false&address=" . $address;
        $data = file_get_contents($url);
        $json = json_decode($data, true);

        if ('OK' == $json['status']) {

            if ($json['results'][0]['geometry']['location_type'] == 'ROOFTOP') {

                $lat = $json['results'][0]['geometry']['location']['lat'];
                $lng = $json['results'][0]['geometry']['location']['lng'];

                return array($lat, $lng);
                exit();
                /*
                 * otestovat
                 */
            } else {
                $return = [
                    'errorCode' => 102,
                    'errorMessage' => 'Address recognized as more places',
                    'places' => [
                        $json['results'][0]['geometry'],
                    ],
                ];
                return $return;
                exit();
            }
        } else {
            $return = [
                'errorCode' => 101,
                'errorMessage' => $json['status'],
            ];

            return $return;
            exit();
        }
    }


    public static function updateEvent(\Illuminate\Http\Request $request, Event $Event){

        $modifiedParameters = array();

        $datetime_now = (new \DateTime());
        $datetime = $datetime_now->sub(new \DateInterval("PT3H"));

        if ($datetime->format('Y-m-d H:i:s') > $Event->date) {

            $response = [
                'errorCode' => 90,
                "message" => "Too late for edition of this fields",
            ];
            return response($response, Response::HTTP_FORBIDDEN);
        }


        if ($request->input('street') || $request->input('city') || $request->input('number') || $request->input('zip')) {
            $response = Event::getCoordinates($request['street'] . ' + ' . $request['number'] . ' + ' . $request['city'] . ' + ' . $request['zip']);

            if (isset($response['errorCode'])) {

                switch ($response['errorCode']) {
                    case 101:
                        return response($response, Response::HTTP_FORBIDDEN);
                        break;
                    case 102:
                        return response($response, Response::HTTP_FORBIDDEN);
                        break;
                    default:
                }
            } else {

                $lat_lon = $response[0] . ',' . $response[1];
                $pluck_pin = Event::distance(500, $lat_lon)->pluck('pin_id');
                //zkusime jestli neexistuje pin blizko naseho bodu

                if ( NULL != $pluck_pin ) {
                    $Event->pin_id = $pluck_pin;
                //zrejme ne tak ulozime novy
                } else {
                    $response = Event::randDistance($response[0], $response[1], 500);
                    $Pin = new Pin();
                    $Pin->gps = $response[0].' '. $response[1];
                    $Pin->save();
                    $Event->pin_id = $Pin->id;

                    }
                /**
                 * zmena adresy, prislusny modifier
                 */
                if ($Event->street != $request->input('street') || $Event->city != $request->input('city') || $Event->number != $request->input('number') || $Event->zip != $request->input('zip')) {
                    $modifiedParameters[] = Notification::MODIFIER_PLACE;
                }

                //update pro adresu
                $Event->street = $request->input('street');
                $Event->city = $request->input('city');
                $Event->number = $request->input('number');
                $Event->zip = $request->input('zip');
                $Event->gps = $response[0] . ' ' . $response[1];
            }
        }


        if (!empty($request->input('name'))) {
            $Event->name = $request->input('name');
        }
        if (!empty($request->input('date'))) {

            $eventDate = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $Event->date);
            $newEventDate = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $request->input('date'));

            /**
             * Zmena datumu
             */
            if ($eventDate->format('Y-m-d') != $newEventDate->format('Y-m-d')) {
                $modifiedParameters[] = Notification::MODIFIER_DATE;
            }

            /**
             * Zmena casu
             */
            if ($eventDate->format('H:i') != $newEventDate->format('H:i')) {
                $modifiedParameters[] = Notification::MODIFIER_TIME;
            }

            $Event->date = $request->input('date');
        }
        if (!empty($request->input('capacity'))) {
            $Event->capacity = $request->input('capacity');
        }
        if ($request->has('indoor')) {
            $Event->indoor = boolval($request->input('indoor'));
        }
        if ($request->has('private')) {
            $Event->private = boolval($request->input('private'));
        }
        if (!empty($request->input('school_id'))) {
            $Event->school_id = $request->input('school_id');
        }
        if (!empty($request['description'])) {
            $Event->description = $request['description'];
        }

        $Event->save();

        /**
         * notifikace pro uzivatele eventu od autora eventu
         */
        //$user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();

        $user_id = $Event->user_id;

        Notification::eventNotification($Event->id, Notification::EVENT_EDITED, $user_id);

        if (!empty($modifiedParameters)) {
            foreach ($modifiedParameters as $modifier) {
                Notification::eventNotification($Event->id, Notification::EVENT_EDITED, $user_id, $modifier);
            }
        }
        return response()->json($Event);
    }
}
?>