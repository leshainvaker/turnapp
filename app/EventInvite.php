<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Auth\User;


class EventInvite extends Model {

    const ACCEPTED = 'INVITE_ACCEPTED';
    const DECLINED = 'INVITE_DECLINED';

    //public $timestamps = false;
    protected $table = 'invite';
    protected $fillable = ['id', 'type', 'event_id', 'user_id'];

    protected $appends = ['user'];

    public function getUserAttribute(){
        if(isset($this->attributes['user_id'])){
            $user = User::where('id', $this->attributes['user_id'])->firstOrFail();
            return $user;
        }
    }

    public function event(){
        return $this->belongsTo('Event');
    }
}
?>
