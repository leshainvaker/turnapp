<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model {
    //public $timestamps = false;
    protected $table = 'rating';
    protected $fillable = ['event_id', 'rating', 'user_id'];
}


?>