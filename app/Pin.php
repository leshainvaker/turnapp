<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use LucaDegasperi\OAuth2Server\Authorizer;
use App\Auth\User;
use App\Event;
use DB;

class Pin extends Model {

    public $timestamps = false;
    protected $table = 'pin';
    protected $fillable = ['id', 'gps'];
    protected $geofields = array('gps');
    protected $appends = ['eventCount', 'indoorEvents', 'outdoorEvents'];

    public function geteventCountAttribute() {

        $datetime_now = (new \DateTime())->format('Y-m-d H:i:s');
        $eventCount = Event::where('event.pin_id', '=', $this->attributes['id'])
                            ->where('event.deleted_at', '=', NULL)
                            ->where('event.date', '>', $datetime_now)
                            ->where(function($query) {
                            $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
                            $user_school = User::where('id', '=', $user_id)->pluck('school');
                            return $query
                                    ->where('event.private', '=', 0)
                                    ->orWhere('event.user_id', '=', $user_id)
                                    ->orWhere('event.school_id', '=', $user_school);
                            })->count();
        return $eventCount;
    }

    public function getindoorEventsAttribute() {

        $indoorEvent = false;
        if (Event::where('pin_id', '=', $this->attributes['id'])->where('indoor', '=', 1)->first()) {
            $indoorEvent = true;
        };
        return $indoorEvent;
    }

    public function getoutdoorEventsAttribute() {
        $outdoorEvents = false;
        if (Event::where('pin_id', '=', $this->attributes['id'])->where('indoor', '=', 0)->first()) {
            $outdoorEvents = true;
        };
        return $outdoorEvents;
    }

    public function newQuery($excludeDeleted = true) {
        $raw = '';
        foreach ($this->geofields as $column) {
            $raw .= ' astext(' . $column . ') as ' . $column . ' ';
        }
        return parent::newQuery($excludeDeleted)->addSelect('pin.*', DB::raw('astext(pin.gps) as gps'));
    }

    public function getGpsAttribute($value) {
        $offset = strrpos($value, 'POINT');
        $loc = substr($value, $offset);
        $loc = substr($loc, 6);
        $loc = preg_replace('/[ ,]+/', ',', $loc, 1);
        $loc = strstr($loc, ')', true);
        $location = explode(",", $loc);
        $loc = $location[1] . ',' . $location[0];
        return $loc;
    }

    public function setGpsAttribute($value) {

        $location = explode(" ", $value);
        $loc = $location[1] . ' ' . $location[0];
        $this->attributes['gps'] = DB::raw("POINTFROMTEXT('POINT($loc)')");
    }

    public function scopeDistance($query, $dist, $location) {
        $location = explode(",", $location);
        $loc = $location[1] . ',' . $location[0];
        return $query->whereRaw('st_distance_sphere(pin.gps,POINT(' . $loc . ')) < ' . $dist);
    }
}
?>