<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserRole extends Model {


    protected $fillable = [
        'role',
        'user_id'
    ];

}

