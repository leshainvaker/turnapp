<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Http\Request;

use App\Auth\User;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserTwitter extends Model {

    protected $table = 'users_twitter';

    protected $fillable = [
        'uid',
        'username',
        'screen_name',
        'email',
    ];


    public static function fromApi(Request $request){

        $stack = HandlerStack::create();

        $middleware = new Oauth1([
            'consumer_key'    => 'WSWzDTX8zhlnavk7tVSRhkBcO',
            'consumer_secret' => 'GGW2IvoxBg1kgsXJ4IonO7pWE9Uw00SVHYD7PElZHCI89DyQcx',
            'token'           => $request->input('twitterToken'),
            'token_secret'    => $request->input('twitterSecret'),
        ]);

        $stack->push($middleware);

        $client = new Client([
            'base_uri' => 'https://api.twitter.com/1.1/',
            'handler' => $stack
        ]);

        // Set the "auth" request option to "oauth" to sign using oauth
        $res = $client->get('account/verify_credentials.json', ['auth' => 'oauth']);

        $contents = $res->getBody()->getContents();

        $twitterUserData = json_decode($contents, true);

        try
        {
            $userTwitter = UserTwitter::where('uid', $twitterUserData['id'])->firstOrFail();

        }
        Catch (ModelNotFoundException $e)
        {

            $userEmail = empty($twitterUserData['email']) ? '' : $twitterUserData['email'];

            $userTwitter = UserTwitter::create([
                'uid' => $twitterUserData['id'],
                'username' => $twitterUserData['name'],
                'screen_name' => $twitterUserData['screen_name'],
                'email' => $userEmail
            ]);
        }


        $userData = $twitterUserData;
        $userData['id_users_twitter'] = $userTwitter->id;

        /**
         *
         */
        /*
        $input = array(
            'grant_type'    => 'twitter_token',
            'id_users_twitter' => $userTwitter->id
        );

        $request->merge($input);
*/

        //$user = User::where('id_users_twitter', $userTwitter->id);

        //if(!($user instanceof User)){

            try
            {

                /*
                if(!empty($userData['email'])){

                    $user = User::where('email', $userTwitter->email)->firstOrFail();

                    if(empty($user->id_users_twitter)){

                        $user->update([
                            'id_users_twitter' => $userTwitter->id
                        ]);

                        $user->save();
                    }
                }*/
                $user = User::where('id_users_twitter', $userTwitter->id)->firstOrFail();;
            }
            Catch (ModelNotFoundException $e)
            {
                //$userEmail = empty($userData['email']) ? '' : $userData['email'];
                $user = User::create([
                    'username' => $userData['name'],
                    //'email' => $userEmail,
                    'id_users_twitter' => $userTwitter->id
                ]);
            }
            return $user;
    }
}