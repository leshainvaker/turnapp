<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class EventPhoto extends Model {

    //protected $table = '';

    protected $fillable = [
        'event_id',
        'original',
        'thumbnail',
        'user_id'
    ];



    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = ['user_id',  'created_at', 'updated_at', 'event_id', 'user_id', 'originalImage', 'thumbnailImage'];
    
    //protected $hidden = ['original', 'thumbnail'];
    
    protected $appends = ['originalImage', 'thumbnailImage'];

    //protected $appends = ['image'];

    
    public function getOriginalImageAttribute(){
        if(isset($this->attributes['original'])){
            return $this->attributes['original'];
        }
    }    
    
    public function getOriginalAttribute(){
        if(!empty($this->original)){
            return route('eventImage', ['image_id' => $this->attributes['id']]) . "?updated=" . strtotime($this->attributes['updated_at']);
        }
    }
    
    public function getThumbnailImageAttribute(){
        //echo 'suck '; exit;
        if(isset($this->attributes['thumbnail'])){
            return $this->attributes['thumbnail'];
        }
        
    }        
    
    public function getThumbnailAttribute(){
        if(!empty($this->attributes['thumbnail'])){
            return route('eventThumbnail', ['image_id' => $this->attributes['id']]) . "?updated=" . strtotime($this->attributes['updated_at']);
        }        
    }    
    
    /**
     *
     * @return type
     */
    /*
    function getImageAttribute() {

      $ret = [
          'original' => $this->attributes['original'],
          'thumbnail' => $this->attributes['thumbnail']
      ];

      return $ret;

    }     */

}