<?php

namespace App\Models;

use App\Event;
use App\EventRequest;
use App\EventInvite;
use App\Auth\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use DB;

class Notification extends Model {

    const THREE_HOURS_BEFORE_START      = "THREE_HOURS_BEFORE_START";
    const CANCELED_EVENT                = "CANCELED_EVENT";
    const NOTE_ADDED                    = "NOTE_ADDED";
    const EVENT_EDITED                  = "EVENT_EDITED";
    const NEW_MESSAGE                   = "NEW_MESSAGE";
    const RECOMMENDATION_TO_EVENT       = "RECOMMENDATION_TO_EVENT";
    const REQUEST_RECEIVED              = "REQUEST_RECEIVED";
    const EVENT_FILLED                  = "EVENT_FILLED";
    const REQUEST_ACCEPTED              = "REQUEST_ACCEPTED";
    const REQUEST_DECLINED              = "REQUEST_DECLINED";
    const INVITATION_RECEIVED           = "INVITE_RECEIVED";
    const INVITATION_ACCEPTED           = "INVITE_ACCEPTED";
    const INVITATION_DECLINED           = "INVITE_DECLINED";
    const CANCEL_REGISTRATION           = "CANCEL_REGISTRATION";


    const MODIFIER_PLACE                = "PLACE";
    const MODIFIER_DATE                 = "DATE";
    const MODIFIER_TIME                 = "TIME";

    protected $table = 'notification';

    protected $fillable = [
        'user_id_to',
        'type',
        'is_read',
        'object_id',
        'user_id_from',
        'modifiedParameter'
    ];

    /**
     * notifikace pro vsechny navstevniky eventu
     *
     * @param type $object_id
     * @param type $type
     */
    public static function eventNotification($event_id, $type, $user_id_from, $modifiedParameter = null){


        /**
         * uzivatele pripsani do eventu. Uzivatele, kteri poslali request. Uzivatele, na ktere ceka pozvanka.
         */
        $sql = '
            INSERT INTO
                notification (type, object_id, user_id_from, user_id_to, modifiedParameter)

            SELECT
                ' . DB::connection()->getPdo()->quote($type) . ', "' . (int) $event_id . '", "' . (int) $user_id_from . '", user_id,' . DB::connection()->getPdo()->quote($modifiedParameter) . '
            FROM
            (
                SELECT
                    uhe.user_id
                FROM
                    user_has_event as uhe
                JOIN
                    event as e1
                ON
                    e1.id = uhe.event_id
                WHERE
                    uhe.event_id = "' . $event_id . '"
                AND
                    uhe.user_has_event = "' . \App\UserHasEvent::VISITOR . '"
                AND
                    e1.date > NOW()

            UNION

                SELECT
                    r.user_id
                FROM
                    request as r
                JOIN
                    event as e2
                ON
                    e2.id = r.event_id
                WHERE
                    r.event_id = "' . $event_id . '"
                AND
                    r.type = "' . Notification::REQUEST_RECEIVED . '"
                AND
                    e2.date > NOW()

            UNION

                SELECT
                    i.user_id
                FROM
                    invite as i
                JOIN
                    event as e3
                ON
                    e3.id = i.event_id
                WHERE
                    i.event_id = "' . $event_id . '"
                AND
                    i.type = "' . Notification::INVITATION_RECEIVED . '"
                AND
                    e3.date > NOW()
            ) t
            GROUP BY
                user_id
        ';

        DB::unprepared($sql);


        /**
         * uzivatele, kteri odeslali request
         */
        /*
        $sql = '
            INSERT INTO
                notification (type, object_id, user_id_from, user_id_to)
            SELECT
                ' . DB::connection()->getPdo()->quote($type) . ', "' . (int) $event_id . '", "' . (int) $user_id_from . '", user_id
            FROM
                request as r
            JOIN
                event as e
            ON
                e.id = r.event_id
            WHERE
                r.event_id = "' . $event_id . '"
            AND
                r.type = "' . Notification::REQUEST_RECEIVED . '"
            AND
                e.date > NOW()
        ';

        DB::unprepared($sql);
        */

        /**
         * uzivatele, kterym dorazila pozvanka
         */
        /*
        $sql = '
            INSERT INTO
                notification (type, object_id, user_id_from, user_id_to)
            SELECT
                ' . DB::connection()->getPdo()->quote($type) . ', "' . (int) $event_id . '", "' . (int) $user_id_from . '", user_id
            FROM
                invite as i
            JOIN
                event as e
            ON
                e.id = i.event_id
            WHERE
                i.event_id = "' . $event_id . '"
            AND
                i.type = "' . Notification::INVITATION_RECEIVED . '"
            AND
                e.date > NOW()
        ';


        DB::unprepared($sql);

         */

    }


    /**
     *
     * @param type $object_id
     * @param type $type
     * @param type $user_id_to
     */
    public static function add($object_id, $type, $user_id_to, $user_id_from, $modifiedParameter = null){

        $data = array(
            'object_id'         =>  $object_id,
            'type'              =>  $type,
            'user_id_to'        =>  $user_id_to,
            'user_id_from'      =>  $user_id_from
        );

        if($modifiedParameter){
            $data['modifiedParameter'] = $modifiedParameter;
        }

        Notification::create($data);

    }


    /**
     * Register a loaded model event with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    /*
    public static function loaded($callback)
    {
        static::registerModelEvent('loaded', $callback);
    }
*/
    /**
     * Get the observable event names.
     *
     * @return array
     */
    /*
    public function getObservableEvents()
    {
        return array_merge(parent::getObservableEvents(), array('loaded'));
    }*/

    /**
     * Create a new model instance that is existing.
     *
     * @param  array  $attributes
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function newFromBuilder($attributes = array(), $connection = NULL)
    {
        $instance = parent::newFromBuilder($attributes);


        switch($instance->type){


            case Notification::NEW_MESSAGE:

                $message = ChatMessage::find($instance->object_id);
                $chat = Chat::find($message->chat_id);

                $instance->chat = $chat;
                break;

            case Notification::CANCEL_REGISTRATION:
                $event = Event::withTrashed()->where('id', $instance->object_id)->first();
                $user = User::find($instance->user_id_from);

                $instance->event = $event;
                $instance->user = $user;
                break;


            case Notification::EVENT_EDITED:
            case Notification::NOTE_ADDED:
            case Notification::EVENT_FILLED:
            case Notification::CANCELED_EVENT:
                $event = Event::withTrashed()->where('id', $instance->object_id)->first();
                $instance->event = $event;
                break;

            case Notification::REQUEST_RECEIVED:
            case Notification::REQUEST_ACCEPTED:
            case Notification::REQUEST_DECLINED:

                try {
                    $event = Event::withTrashed()->join('request', 'event.id', '=', 'request.event_id')
                                    ->join('notification', 'notification.object_id', '=', 'request.id')
                                    ->where('notification.id', '=', $instance->id)
                                    ->select('event.id', 'event.user_id')
                                    ->firstOrFail();

                    //var_dump($event->id);

                    //$eventRequest = EventRequest::where('id', $instance->object_id)->first();
                    //$event = Event::find(30);

                    $event = Event::withTrashed()->where('id', $event->id)->firstOrFail();
                    $instance->event = $event;
                }
                Catch (ModelNotFoundException $e){

                }
                break;

            case Notification::INVITATION_RECEIVED:
            case Notification::INVITATION_ACCEPTED:
            case Notification::INVITATION_DECLINED:

                /*
                $event = Event::withTrashed()->join('invite', 'event.id', '=', 'invite.event_id')
                                ->join('notification', 'notification.object_id', '=', 'invite.id')
                                ->where('notification.id', '=', $instance->id)
                                ->select('event.id', 'event.user_id')
                                ->first();
                */


                try {
                    $invitation = EventInvite::join('notification', 'notification.object_id', '=', 'invite.id')
                        ->where('notification.id', '=', $instance->id)
                        ->select('invite.event_id')
                        ->firstOrFail();


                    $event = Event::withTrashed()->where('id', $invitation->event_id)->firstOrFail();
                    $instance->event = $event;
                }
                Catch (ModelNotFoundException $e){

                }
                break;

            case Notification::RECOMMENDATION_TO_EVENT:


                /*
                $event = Event::withTrashed()->join('event_recommends', 'event.id', '=', 'event_recommends.event_id')
                                ->join('notification', 'notification.object_id', '=', 'event_recommends.id')
                                ->where('notification.id', '=', $instance->id)
                                ->select('event.id', 'event.user_id')
                                ->first();
                */

                $user = User::join('event_recommends', 'users.id','=', 'event_recommends.user_id_from')
                        ->join('notification', 'notification.object_id', '=', 'event_recommends.id')
                        ->where('notification.id', '=', $instance->id)
                        ->select('users.*')
                        ->first();


                try { 
                    
                    $event = Event::withTrashed()->join('event_recommends', 'event.id', '=', 'event_recommends.event_id')
                                ->join('notification', 'notification.object_id', '=', 'event_recommends.id')
                                ->where('notification.id', '=', $instance->id)
                                ->select('event.id', 'event.user_id')
                                ->firstOrFail();                    
                    
                    $event = Event::withTrashed()->where('id', $event->id)->firstOrFail();
                    $instance->event = $event;
                }
                Catch (ModelNotFoundException $e){

                }                
                
                $instance->user = $user;
                break;


            case Notification::THREE_HOURS_BEFORE_START:




                break;

        }
        return $instance;
    }
}