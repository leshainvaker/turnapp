<?php

namespace App\Models;

use App\Models\ChatMessage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Chat extends Model {

    protected $table = 'chat';

    protected $fillable = [
        'user_id_from',
        'user_id_to'
    ];


    protected $hidden = ['user_id_from', 'user_id_to'];

    protected $appends = ['user', 'unreadMesasges', 'last_message'];

    public function getLastMessageAttribute(){
        
        $lastMessage = \App\Models\ChatMessage::where('chat_id', $this->attributes['id'])->orderBy('id', 'desc')->first();
        return $lastMessage;
        
    }
    
    public function getUnreadMesasgesAttribute(){
        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
        $unreadMessages = ChatMessage::Where('chat_id', '=', $this->attributes['id'])->where('user_id', '!=' , $user_id)->where('message_state', ChatMessage::STATE_UNREAD)->count();
        //$unreadMessages = ChatMessage::where('user_id', '!=' , $user_id)->andWhere('chat_id', '=', 7)->count();
        return $unreadMessages;
    }

    public function user(){
        return $this->hasOne('App\Auth\User', 'id', 'user_id_from');
    }
 
    public function getUserAttribute()
    {
        $user_id = app()->make('oauth2-server.authorizer')->getResourceOwnerId();
        $local_key = $user_id == $this->attributes['user_id_from'] ? 'user_id_to' : 'user_id_from';
        return $this->hasOne('App\Auth\User', 'id', $local_key)->get();
    }
}