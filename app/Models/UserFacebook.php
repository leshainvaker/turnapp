<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use Illuminate\Http\Request;

use App\Auth\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;



class UserFacebook extends Model {
    
    protected $table = 'users_facebook';    
    
    protected $fillable = [
        'uid',
        'email',
        'username'
    ];
    
    public static function fromApi(Request $request){
        
        $accessToken = $request->input('facebookAcessToken');
        $url = "https://graph.facebook.com/me?access_token=" . $accessToken;

        $userDetail = file_get_contents($url);
        $facebookUserData = json_decode($userDetail, true);

        // jestlize email neni v tabulce users
        $facebookUserData['uid'] = $facebookUserData['id'];
        unset($facebookUserData['id']);
        $facebookUserData['username'] = $facebookUserData['name'];

        // kouknu jestli jiz existuje registrace, pres facebook
        try
        {
            $facebookUser = UserFacebook::where('uid', $facebookUserData['uid'])->firstOrFail();

            // @TO DO
            // shoul we update data from facebook?

        }
        Catch (ModelNotFoundException $e)
        {
            $facebookUser = UserFacebook::create($facebookUserData);
        }



        $userData = $facebookUserData;
        $userData['id_users_facebook'] = $facebookUser->id;
        
        /**
         * 
         */
        $input = array(            
            'id_users_facebook' => $facebookUser->id
        );
        
        $request->merge($input);

        try
        {
            //$user = User::where('id_users_facebook', $facebookUser->id)->firstOrFail();
            
            if(!empty($facebookUserData['email'])){            
                $user = User::where('email', $facebookUserData['email'])->firstOrFail();
            

                if(empty($user->id_users_facebook)){

                    $user->update([
                        'id_users_facebook' => $facebookUser->id
                    ]);

                    $user->save();
                }
            }
            else {
                $user = User::where('id_users_facebook', $facebookUser->id)->firstOrFail();
            }
            
        }
        Catch (ModelNotFoundException $e)
        {
            $user = User::create($userData);
        }

        return $user;        
        
    }
    
}
