<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class School extends Model {
    
    protected $table = 'school';
    
    protected $fillable = [
        'name',
        'event_id',
        'user_id'
    ];    
    
}
