<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EventRecommend extends Model {
    
    protected $fillable = [
        'event_id',
        'user_id_from',
        'user_id_to'
    ];    
    
}