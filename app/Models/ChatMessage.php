<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ChatMessage extends Model {
    
    //protected $table = 'chat_message';
    const STATE_UNREAD = 0;
    const STATE_READ = 1; 
    
    protected $fillable = [
        'message',
        'subject',        
        'chat_id',
        'user_id',
        'message_state'
    ];    
    
    protected $hidden = ['updated_at', 'created_at', 'message_state'];
    
    protected $appends = ['created', 'fromMe'];
    
    function getCreatedAttribute() {
        return $this->attributes['created_at'];      
    }     
    
    function getFromMeattribute() {      
        $id_current_user = app()->make('oauth2-server.authorizer')->getResourceOwnerId();        
        $fromMe = $id_current_user == $this->attributes['user_id'] ? 1 : 0;        
        return (int) $fromMe;      
    }         
    
}