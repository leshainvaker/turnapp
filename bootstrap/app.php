<?php

require_once __DIR__.'/../vendor/autoload.php';
//require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

$app->withFacades();

$app->withEloquent();

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

// vykona se v kazdem pozadavku
$app->middleware([    
    App\Http\Middleware\OauthClient::class,
    //LucaDegasperi\OAuth2Server\Middleware\OAuthExceptionHandlerMiddleware::class,
    
    //App\Http\Middleware\OAuthExceptionHandlerMiddleware::class,
    Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
    App\Http\Middleware\ExpandToken::class,
    //Optimus\OAuth2Server\Middleware\OAuthMiddleware::class
]);

// vykona se v pozadavku, ktery specifikujeme
$app->routeMiddleware([
    'api-key' => App\Http\Middleware\ApiKey::class,
    'check-authorization-params' => Optimus\OAuth2Server\Middleware\CheckAuthCodeRequestMiddleware::class,
    'csrf' => Laravel\Lumen\Http\Middleware\VerifyCsrfToken::class,
    'oauth' => Optimus\OAuth2Server\Middleware\OAuthMiddleware::class,
    //'oauth-owner' => Optimus\OAuth2Server\Middleware\OAuthOwnerMiddleware::class
    'oauth-client' => \LucaDegasperi\OAuth2Server\Middleware\OAuthClientOwnerMiddleware::class,
    'oauth-user' => \LucaDegasperi\OAuth2Server\Middleware\OAuthUserOwnerMiddleware::class,    
    'oauth-exception' => App\Http\Middleware\OAuthExceptionHandlerMiddleware::class,
    'adminAuth' => App\Http\Middleware\AdminAuth::class,
    'adminAccess' => App\Http\Middleware\AdminAccess::class
        
        
    
]);

// $app->middleware([
//    App\Http\Middleware\ExampleMiddleware::class
// ]);

// $app->routeMiddleware([
//     'auth' => App\Http\Middleware\Authenticate::class,
// ]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

//$app->register('LucaDegasperi\OAuth2Server\Storage\FluentStorageServiceProvider');
//$app->register('Optimus\OAuth2Server\OAuth2ServerServiceProvider');

$app->register(LucaDegasperi\OAuth2Server\Storage\FluentStorageServiceProvider::class);
$app->register(Optimus\OAuth2Server\OAuth2ServerServiceProvider::class);
$app->register(Intervention\Image\ImageServiceProvider::class);

//'Intervention\Image\ImageServiceProvider'
//$app->register(Laravel\Socialite\SocialiteServiceProvider::class);

// https://github.com/lucadegasperi/oauth2-server-laravel/blob/master/docs/getting-started/lumen.md#register-package
//$app->register(LucaDegasperi\OAuth2Server\OAuth2ServerServiceProvider::class);

// $app->register(App\Providers\AppServiceProvider::class);
// $app->register(App\Providers\AuthServiceProvider::class);
// $app->register(App\Providers\EventServiceProvider::class);

//$app->register(App\Providers\AuthServiceProvider::class);

$app->register(App\Providers\AppServiceProvider::class);
$app->register(Davibennun\LaravelPushNotification\LaravelPushNotificationServiceProvider::class);


$app->register(LaravelFCM\FCMServiceProvider::class);
$app->register(Appzcoder\LumenRoutesList\RoutesCommandServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->group(['namespace' => 'App\Http\Controllers'], function ($app) {
    require __DIR__.'/../app/Http/routes.php';
});

$app->bind(Illuminate\Contracts\Cookie\QueueingFactory::class, Illuminate\Cookie\CookieJar::class);


class_alias(Davibennun\LaravelPushNotification\Facades\PushNotification::class, 'PushNotification');
class_alias(Intervention\Image\Facades\Image::class, 'Image');
class_alias(LucaDegasperi\OAuth2Server\Facades\AuthorizerFacade::class, 'Authorizer');

class_alias(\LaravelFCM\Facades\FCM::class, 'FCM');
class_alias(\LaravelFCM\Facades\FCMGroup::class, 'FCMGroup');


//$app->bind('Image', Intervention\Image\Facades\Image::class);

//'Image' => 'Intervention\Image\Facades\Image'

$app->configure('app');
$app->configure('secrets');

$app->configure('push-notification');



return $app;
