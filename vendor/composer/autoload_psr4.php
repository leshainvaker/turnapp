<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'phpDocumentor\\Reflection\\' => array($vendorDir . '/phpdocumentor/reflection-common/src', $vendorDir . '/phpdocumentor/type-resolver/src', $vendorDir . '/phpdocumentor/reflection-docblock/src'),
    'Zend\\Validator\\' => array($vendorDir . '/zendframework/zend-validator/src'),
    'Zend\\Uri\\' => array($vendorDir . '/zendframework/zend-uri/src'),
    'Zend\\Stdlib\\' => array($vendorDir . '/zendframework/zend-stdlib/src'),
    'Zend\\Loader\\' => array($vendorDir . '/zendframework/zend-loader/src'),
    'Zend\\Json\\' => array($vendorDir . '/zendframework/zend-json/src'),
    'Zend\\Http\\' => array($vendorDir . '/zendframework/zend-http/src'),
    'Zend\\Escaper\\' => array($vendorDir . '/zendframework/zend-escaper/src'),
    'Webmozart\\Assert\\' => array($vendorDir . '/webmozart/assert/src'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Component\\Yaml\\' => array($vendorDir . '/symfony/yaml'),
    'Symfony\\Component\\VarDumper\\' => array($vendorDir . '/symfony/var-dumper'),
    'Symfony\\Component\\Translation\\' => array($vendorDir . '/symfony/translation'),
    'Symfony\\Component\\Process\\' => array($vendorDir . '/symfony/process'),
    'Symfony\\Component\\OptionsResolver\\' => array($vendorDir . '/symfony/options-resolver'),
    'Symfony\\Component\\HttpKernel\\' => array($vendorDir . '/symfony/http-kernel'),
    'Symfony\\Component\\HttpFoundation\\' => array($vendorDir . '/symfony/http-foundation'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'Symfony\\Component\\DomCrawler\\' => array($vendorDir . '/symfony/dom-crawler'),
    'Symfony\\Component\\Debug\\' => array($vendorDir . '/symfony/debug'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'Stringy\\' => array($vendorDir . '/danielstjules/stringy/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'Optimus\\OAuth2Server\\' => array($vendorDir . '/optimus/oauth2-server-lumen/src'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
    'LucaDegasperi\\OAuth2Server\\' => array($vendorDir . '/lucadegasperi/oauth2-server-laravel/src'),
    'League\\OAuth2\\Server\\' => array($vendorDir . '/league/oauth2-server/src'),
    'League\\Event\\' => array($vendorDir . '/league/event/src'),
    'Laravel\\Lumen\\' => array($vendorDir . '/laravel/lumen-framework/src'),
    'LaravelFCM\\Mocks\\' => array($vendorDir . '/brozot/laravel-fcm/tests/mocks'),
    'LaravelFCM\\' => array($vendorDir . '/brozot/laravel-fcm/src'),
    'Intervention\\Image\\' => array($vendorDir . '/intervention/image/src/Intervention/Image'),
    'Interop\\Container\\' => array($vendorDir . '/container-interop/container-interop/src/Interop/Container'),
    'Illuminate\\View\\' => array($vendorDir . '/illuminate/view'),
    'Illuminate\\Validation\\' => array($vendorDir . '/illuminate/validation'),
    'Illuminate\\Translation\\' => array($vendorDir . '/illuminate/translation'),
    'Illuminate\\Support\\' => array($vendorDir . '/illuminate/support'),
    'Illuminate\\Session\\' => array($vendorDir . '/illuminate/session'),
    'Illuminate\\Queue\\' => array($vendorDir . '/illuminate/queue'),
    'Illuminate\\Pipeline\\' => array($vendorDir . '/illuminate/pipeline'),
    'Illuminate\\Pagination\\' => array($vendorDir . '/illuminate/pagination'),
    'Illuminate\\Mail\\' => array($vendorDir . '/illuminate/mail'),
    'Illuminate\\Http\\' => array($vendorDir . '/illuminate/http'),
    'Illuminate\\Hashing\\' => array($vendorDir . '/illuminate/hashing'),
    'Illuminate\\Filesystem\\' => array($vendorDir . '/illuminate/filesystem'),
    'Illuminate\\Events\\' => array($vendorDir . '/illuminate/events'),
    'Illuminate\\Encryption\\' => array($vendorDir . '/illuminate/encryption'),
    'Illuminate\\Database\\' => array($vendorDir . '/illuminate/database'),
    'Illuminate\\Cookie\\' => array($vendorDir . '/illuminate/cookie'),
    'Illuminate\\Contracts\\' => array($vendorDir . '/illuminate/contracts'),
    'Illuminate\\Container\\' => array($vendorDir . '/illuminate/container'),
    'Illuminate\\Console\\' => array($vendorDir . '/illuminate/console'),
    'Illuminate\\Config\\' => array($vendorDir . '/illuminate/config'),
    'Illuminate\\Cache\\' => array($vendorDir . '/illuminate/cache'),
    'Illuminate\\Bus\\' => array($vendorDir . '/illuminate/bus'),
    'Illuminate\\Broadcasting\\' => array($vendorDir . '/illuminate/broadcasting'),
    'Illuminate\\Auth\\' => array($vendorDir . '/illuminate/auth'),
    'GuzzleHttp\\Subscriber\\Oauth\\' => array($vendorDir . '/guzzlehttp/oauth-subscriber/src'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'FastRoute\\' => array($vendorDir . '/nikic/fast-route/src'),
    'Faker\\' => array($vendorDir . '/fzaninotto/faker/src/Faker'),
    'Dotenv\\' => array($vendorDir . '/vlucas/phpdotenv/src'),
    'Doctrine\\Instantiator\\' => array($vendorDir . '/doctrine/instantiator/src/Doctrine/Instantiator'),
    'Doctrine\\Common\\Cache\\' => array($vendorDir . '/doctrine/cache/lib/Doctrine/Common/Cache'),
    'Doctrine\\Common\\' => array($vendorDir . '/doctrine/common/lib/Doctrine/Common'),
    'Carbon\\' => array($vendorDir . '/nesbot/carbon/src/Carbon'),
    'Appzcoder\\LumenRoutesList\\' => array($vendorDir . '/appzcoder/lumen-routes-list/src'),
    'App\\' => array($baseDir . '/app'),
);
